<?php
ob_start();
session_start();
require('../_app/Config.inc.php');
//require('../pdf/Mpdf.php');

$login = new Login(3);
$logoff = filter_input(INPUT_GET, 'logoff', FILTER_VALIDATE_BOOLEAN);
$getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);


if (!$login->CheckLogin()):
    unset($_SESSION['userlogin']);
    header('Location: index.php?exe=restrito');
else:
    $userlogin = $_SESSION['userlogin'];
endif;

if ($logoff):
    unset($_SESSION['userlogin']);
    header('Location: index.php?exe=logoff');
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <title>Painel Administrativo <?= SITENAME . ' ' . SITEDESC?></title>
        <!--[if lt IE 9]>
                   <script src="../_cdn/html5.js"></script> 
                <![endif]-->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/reset.css" />
        <link rel="stylesheet" href="css/admin.css" />   
    </head>

    <body class="painel">
        <header id="navadmin">
            <div class="content">
                <?php 
                $readLogo = new Read;
                $readLogo->ExeRead('imagens', "WHERE img_nome = :nome", "nome=logo");
                if ($readLogo->getResult()):
                    ?>
                <img src="<?= HOME ?>/uploads/<?= $readLogo->getResult()[0]['img_img']?>">
                <?php
                endif;
                ?>
                

                <ul class="systema_nav radius">
                    <li class="username">Olá <?= $userlogin['user_name']; ?> <?= $userlogin['user_lastname']; ?></li>
                    <li><a class="icon profile radius" href="painel.php?exe=users/profile">Perfíl</a></li>
                    <li><a class="icon users radius" href="painel.php?exe=users/users">Usuários</a></li>
                    <li><a class="icon logout radius" href="painel.php?logoff=true">Sair</a></li>
                </ul>
            </div><!--/CONTENT-->
        </header>


        <section class="container-painel">
            <div class="navegacao">
                <nav class="menu">
                     <?php
                    //ATIVA MENU
                    if (isset($getexe)):
                        $linkto = explode('/', $getexe);
                    else:
                        $linkto = array();
                    endif;
                    ?>
                    
                    <h1><a href="painel.php" title="Dasboard">HOME</a></h1>
                    
                    <div class="icones">
                        <a href="painel.php?exe=categorias/index" class="link <?php if (in_array('categorias', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Categorias</h4>
                        </a>
                        <a href="painel.php?exe=posts/index" class="link <?php if (in_array('posts', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Canhotos</h4>
                        </a>
                       
                        <a href="painel.php?exe=empresa/index" class="link <?php if (in_array('empresa', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Sobre</h4>
                        </a>
                        
                        <a href="painel.php?exe=info/index" class="link <?php if (in_array('info', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Informações Área Restrita</h4>
                        </a>
                        
                        <a href="painel.php?exe=imagens/index" class="link <?php if (in_array('imagens', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Logo da Empresa</h4>
                        </a>
                        
                        <a href="painel.php?exe=banner/index" class="link <?php if (in_array('banner', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Banner</h4>
                        </a>
                        
                        <a href="painel.php?exe=contato/index" class="link <?php if (in_array('contato', $linkto)) echo ' active'; ?>">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Contato</h4>
                        </a>
                        <a href="<?= HOME ?>" target="_blank" class="link">
                            <span><img src="icons/act_ative.png"></span>
                            <h4>Ver Site</h4>
                        </a>
                    </div>
                </nav>
            </div>

            <div class="informacoes">
                <?php
                //QUERY STRING
                if (!empty($getexe)):
                    $includepatch = __DIR__ . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . strip_tags(trim($getexe) . '.php');
                else:
                    $includepatch = __DIR__ . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR . 'home.php';
                endif;

                if (file_exists($includepatch)):
                    require_once($includepatch);
                else:
                    echo "<div class=\"content notfound\">";
                    WSErro("<b>Erro ao incluir tela: </b>Erro ao incluir o controller /{$getexe}.php!", WS_ERROR);
                    echo "</div>";
                endif;
                ?>
            </div> <!-- painel -->
        </section>

        <footer class="main_footer">
            <a href="http://www.fladermorais.com.br" target="_blank" title="Flader Morais">&copy; Flader Morais - Todos os Direitos Reservados</a>
        </footer>

    </body>

    <script src="../_cdn/jquery.js"></script>
    <script src="../_cdn/jmask.js"></script>
    <script src="../_cdn/combo.js"></script>
    <script src="../_cdn/app.js"></script>
    <script src="__jsc/tiny_mce/tiny_mce.js"></script>
    <script src="__jsc/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
    <script src="__jsc/admin.js"></script>


</html>
<?php
ob_end_flush();
