<?php

/**
 * AdminPost.class [ MODEL ADMIN ]
 * Respnsável por gerenciar as posts no Admin do sistema!
 * 
 * @copyright (c) 2017, Flader Morais - Soluções em Informática
 */
class AdminImagens {

    private $Data;
    private $Post;
    private $Error;
    private $Result;

    //Nome da tabela no banco de dados
    const Entity = 'imagens';

    /**
     * <b>Cadastrar o Post:</b> Envelope os dados do post em um array atribuitivo e execute esse método
     * para cadastrar o post. Envia a capa automaticamente!
     * @param ARRAY $Data = Atribuitivo
     */
    public function ExeCreate(array $Data) {
        $this->Data = $Data;

        if (in_array('', $this->Data)):
            $this->Error = ["Erro ao cadastrar: Para cadastrar uma nova imagem é necessário preencher todos os campos!", WS_ALERT];
            $this->Result = false;
        else:
            $this->setData();
            $this->setName();

            if ($this->Data['img_img']):
                $uplaod = new Upload;
                $uplaod->Image($this->Data['img_img'], $this->Data['img_nome']);
            endif;

            if (isset($uplaod) && $uplaod->getResult()):
                $this->Data['img_img'] = $uplaod->getResult();
                $this->Create();
            else:
                $this->Data['img_img'] = null;
                $_SESSION['errCapa'] = "<b>ERRO AO ENVIAR IMAGEM</b> Tipo de arquivo inválido, envie imagens JPG ou PNG!";
                $this->Create();
            endif;
        endif;
    }

    /**
     * <b>Atualizar Post:</b> Envelope os dados em uma array atribuitivo e informe o id de um 
     * post para atualiza-lo na tabela!
     * @param INT $PostId = Id do post
     * @param ARRAY $Data = Atribuitivo
     */
    public function ExeUpdate($PostId, array $Data) {
        $this->Post = (int) $PostId;
        $this->Data = $Data;

        if (in_array('', $this->Data)):
            $this->Error = ["Para atualizar esta imagem é necessário preencher todos os campos ( Imagem não precisa ser enviada! )", WS_ALERT];
            $this->Result = false;
        else:
            $this->setData();
            $this->setName();

            if (is_array($this->Data['img_img'])):
                $readCapa = new Read;
                $readCapa->ExeRead(self::Entity, "WHERE img_id = :post", "post={$this->Post}");
                $capa = '../uploads/' . $readCapa->getResult()[0]['img_img'];
                if (file_exists($capa) && !is_dir($capa)):
                    unlink($capa);
                endif;

                $uploadCapa = new Upload;
                $uploadCapa->Image($this->Data['img_img'], $this->Data['img_nome']);
            endif;

            if (isset($uploadCapa) && $uploadCapa->getResult()):
                $this->Data['img_img'] = $uploadCapa->getResult();
                $this->Update();
            else:
                unset($this->Data['img_img']);
                if (!empty($uploadCapa) && $uploadCapa->getError()):
                    WSErro("<b>ERRO AO ENVIAR IMAGEM </b> " . $uploadCapa->getError(), E_USER_WARNING);
                endif;
                $this->Update();
            endif;
        endif;
    }

    /**
     * <b>Deleta Post:</b> Informe o ID do post a ser removido para que esse método realize uma checagem de
     * pastas e galerias excluinto todos os dados nessesários!
     * @param INT $PostId = Id do post
     */
    public function ExeDelete($PostId) {
        $this->Post = (int) $PostId;

        $ReadPost = new Read;
        $ReadPost->ExeRead(self::Entity, "WHERE img_id = :post", "post={$this->Post}");

        if (!$ReadPost->getResult()):
            $this->Error = ["A Imagem  que você tentou deletar não existe no sistema!", WS_ERROR];
            $this->Result = false;
        else:
            $PostDelete = $ReadPost->getResult()[0];
            if (file_exists('../uploads/' . $PostDelete['img_img']) && !is_dir('../uploads/' . $PostDelete['img_img'])):
                unlink('../uploads/' . $PostDelete['img_img']);
            endif;

            $deleta = new Delete;
            $deleta->ExeDelete(self::Entity, "WHERE img_id = :postid", "postid={$this->Post}");

            $this->Error = ["A Imagem <b>{$PostDelete['img_titulo']}</b> foi removido com sucesso do sistema!", WS_ACCEPT];
            $this->Result = true;

        endif;
    }
    
     /**
     * <b>Ativa/Inativa Post:</b> Informe o ID do post e o status e um status sendo 1 para ativo e 0 para
     * rascunho. Esse méto ativa e inativa os posts!
     * @param INT $PostId = Id do post
     * @param STRING $PostStatus = 1 para ativo, 0 para inativo
     */
    public function ExeStatus($PostId, $PostStatus) {
        $this->Post = (int) $PostId;
        $this->Data['img_status'] = (string) $PostStatus;
        $Update = new Update;
        $Update->ExeUpdate(self::Entity, $this->Data, "WHERE img_id = :id", "id={$this->Post}");
    }
    
    
    /**
     * <b>Verificar Cadastro:</b> Retorna ID do registro se o cadastro for efetuado ou FALSE se não.
     * Para verificar erros execute um getError();
     * @return BOOL $Var = InsertID or False
     */
    public function getResult() {
        return $this->Result;
    }

    /**
     * <b>Obter Erro:</b> Retorna um array associativo com uma mensagem e o tipo de erro.
     * @return ARRAY $Error = Array associatico com o erro
     */
    public function getError() {
        return $this->Error;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    //Valida e cria os dados para realizar o cadastro
    private function setData() {
        $Cover = $this->Data['img_img'];
        unset($this->Data['img_img']);

        $this->Data = array_map('strip_tags', $this->Data);
        $this->Data = array_map('trim', $this->Data);

        $this->Data['img_nome'] = Check::Name($this->Data['img_titulo']);
        $this->Data['img_img'] = $Cover;
    }

    //Verifica o NAME post. Se existir adiciona um pós-fix -Count
    private function setName() {
        $Where = (isset($this->Post) ? "img_id != {$this->Post} AND" : '');
        $readName = new Read;
        $readName->ExeRead(self::Entity, "WHERE {$Where} img_titulo = :t", "t={$this->Data['img_titulo']}");
        if ($readName->getResult()):
            $this->Data['img_nome'] = $this->Data['img_nome'] . '-' . $readName->getRowCount();
        endif;
    }

    //Cadastra o post no banco!
    private function Create() {
        $cadastra = new Create;
        $cadastra->ExeCreate(self::Entity, $this->Data);
        if ($cadastra->getResult()):
            $this->Error = ["A Imagem <b>{$this->Data['img_titulo']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT];
            $this->Result = $cadastra->getResult();
        endif;
    }

    //Atualiza o post no banco!
    private function Update() {
        $Update = new Update;
        $Update->ExeUpdate(self::Entity, $this->Data, "WHERE img_id = :id", "id={$this->Post}");
        if ($Update->getResult()):
            $this->Error = ["A Imagem <b>{$this->Data['img_titulo']}</b> foi atualizado com sucesso no sistema!", WS_ACCEPT];
            $this->Result = true;
        endif;
    }
}