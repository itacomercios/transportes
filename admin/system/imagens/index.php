<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="box-documentos">
    <section class="conteudo_lista">
        <div class="conteudo_lista_menu">
            <!--<a class="btn blue" href="painel.php?exe=imagens/create">Cadastrar Imagens</a>-->
        </div>

        <h1 class="boxtitle">Imagens:</h1>
        <section>
            <div class="titulo_da_coluna">
                <div>
                    <p>Imagem</p>
                </div>
                <p>Nome</p>
                <!--<p>Downloads</p>-->
                <div>
                    <p>Ações</p>
                </div>
            </div>
            <?php
            $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
            if ($empty):
                WSErro("Oppsss: Você tentou editar uma imagem que não existe no sistema!", WS_INFOR);
            endif;

            // Começo do Action para ver se Ativa, desativa ou deleta o post
            $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
            if ($action):
                require ('_models/AdminImagens.class.php');
                $postAction = filter_input(INPUT_GET, 'post', FILTER_VALIDATE_INT);
                $postUpdate = new AdminImagens;
                switch ($action):
                    case 'active':
                        $postUpdate->ExeStatus($postAction, '1');
                        WSErro("O status do documento foi atualizado para <b>ativo</b>. Documento está disponível para download!", WS_ACCEPT);
                        break;
                    case 'inative':
                        $postUpdate->ExeStatus($postAction, '0');
                        WSErro("O status do documento foi atualizado para <b>inativo</b>. Ele não estará disponível para o usuário final!", WS_ALERT);
                        break;
                    case 'delete':
                        $postUpdate->ExeDelete($postAction);
                        WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                        break;
                    default :
                        WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
                endswitch;
            endif;
            // Fim do Action para ver se Ativa, desativa ou deleta o post  

            $posti = 0;
            $readPosts = new Read;
            $readPosts->ExeRead("imagens");
            if ($readPosts->getResult()):
                foreach ($readPosts->getResult() as $post):
                    $posti++;
                    extract($post);
                    $status = (!$img_status ? 'style="background: #fffed8"' : '');
                    ?>
                    <div <?php echo ' class="detalhes"'; ?> <?= $status; ?>>
                        <div class="img">
                            <img src="<?= HOME . '/uploads/' . $img_img ?>">
                        </div>
                        <p><?= Check::Words($img_titulo, 10) ?></p>


                        <div class="post_actions">
                            <a class="act_edit" href="painel.php?exe=imagens/update&postid=<?= $img_id; ?>" title="Editar">Editar</a>

                            <!--                             <?php if (!$img_status): ?>
                                                                    <a class="act_inative" href="painel.php?exe=imagens/index&post=<?= $img_id; ?>&action=active" title="Ativar">Ativar</a>
                            <?php else: ?>
                                                                    <a class="act_ative" href="painel.php?exe=imagens/index&post=<?= $img_id; ?>&action=inative" title="Inativar">Inativar</a>
                            <?php endif; ?> -->

                                            <!--<a class="act_delete" href="painel.php?exe=imagens/index&post=<?= $img_id; ?>&action=delete" title="Excluir">Deletar</a>-->
                        </div>
                    </div>
                    <?php
                endforeach;

            else:
//                    $Pager->ReturnPage();
                WSErro("Desculpe, Não existe nenhum documento cadastrado para este usuário!<br><br> Se essa for a página principal, selecione um usuário ao lado para poder exibir os documentos relacionados ao mesmo!", WS_INFOR);
            endif;
            ?>
        </section>
    </section>   
</div>