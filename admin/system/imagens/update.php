<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <header>
        <h1>Atualizar Imagem:</h1>
    </header>

    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $postid = filter_input(INPUT_GET, 'postid', FILTER_VALIDATE_INT);

    if (isset($post) && $post['SendPostForm']):
        $post['img_img'] = ( $_FILES['img_img']['tmp_name'] ? $_FILES['img_img'] : 'null' );
        unset($post['SendPostForm']);

        require('_models/AdminImagens.class.php');
        $cadastra = new AdminImagens;
        $cadastra->ExeUpdate($postid, $post);

        header('Location: painel.php?exe=imagens/index');
        WSErro($cadastra->getError()[0], $cadastra->getError()[1]);


    else:
        $read = new Read;
        $read->ExeRead("imagens", "WHERE img_id = :id", "id={$postid}");
        if (!$read->getResult()):
            header('Location: painel.php?exe=imagens/index&empty=true');
        else:
            $post = $read->getResult()[0];
        endif;
    endif;

    if (!empty($_SESSION['errCapa'])):
        WSErro($_SESSION['errCapa'], E_USER_WARNING);
        unset($_SESSION['errCapa']);
    endif;

    $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
    if ($checkCreate && empty($cadastra)):
        WSErro("A Imagem <b>{$post['img_titulo']}</b> foi atualizado com sucesso no sistema!", WS_ACCEPT);
    endif;
    ?>


    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <label class="label">
            <span class="field">Imagem</span>
            <input type="file" name="img_img" />
        </label>

        <label class="label">
            <span class="field">IMAGEM: </span>
            <input readonly type="text" name="img_titulo" value="<?php if (isset($post['img_titulo'])) echo $post['img_titulo']; ?>" />
        </label>

        <div class="label_line">

            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Atualizar" name="SendPostForm" />
        </div>
    </form>
</div> <!-- content home -->