<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="form_create">
    <h1 class="boxtitle">Cadastrar Canhoto de NF-e</h1>

    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if (isset($post) && $post['SendPostForm']):
        $post['img_status'] = ($post['SendPostForm'] == 'Cadastrar' ? '0' : '1' );
        $post['img_img'] = ( $_FILES['img_img']['tmp_name'] ? $_FILES['img_img'] : null );
        unset($post['SendPostForm']);

        require('_models/AdminImagens.class.php');
        $cadastra = new AdminImagens;
        $cadastra->ExeCreate($post);

        if ($cadastra->getResult()):
            header('Location: painel.php?exe=imagens/index');
        else:
            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
        endif;
    endif;
    ?>


    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <label class="label">
            <span class="field">Imagem do Canhoto</span>
            <input type="file" name="img_img" />
        </label>

        <label class="label">
            <span class="field">IMAGEM: </span>
            <input type="text" name="img_titulo" value="<?php if (isset($post['img_titulo'])) echo $post['img_titulo']; ?>" />
        </label>

        <div class="label_line">

            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Publicar" name="SendPostForm" />
        </div>
    </form>
</div> <!-- content home -->