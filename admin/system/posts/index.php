<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="box-documentos">
    <?php include "sidebar.php"; ?>

    <section class="conteudo_lista">
        <div>
            <div class="conteudo_lista_menu">
                <div class="pesquisa">
                    <h1 class="boxtitle">Filtre pelo Nº NF-e</h1>
                    <?php
                    $search = filter_input(INPUT_POST, 'search-texto', FILTER_DEFAULT);
                    if (!empty($search)):
                        $search = strip_tags(trim(urlencode($search)));
                        header('Location: painel.php?exe=posts/pesquisa&filtro=' . $search);
//                    header('Location: ' . HOME . '/pesquisa/' . $search);
                    endif;
                    ?>
                    <form name="search" action="" method="post">
                        <input class = "fls" type = "text" name = "search-texto" placeholder="Procure pelo número da NF-e!"/>
                        <input class = "btn" type = "submit" name = "sendsearch" value = "" />
                    </form>
                </div>
                <div class="filtro-pesquisa">
                    <h1 class="boxtitle">Filtre por período</h1>
                    <?php
                    $filtro = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($filtro) && $filtro['filtrar']):
                        $get = filter_input(INPUT_GET, 'filtro', FILTER_DEFAULT);
                        $datainicio = $filtro['data-inicio'];
                        $datafim = $filtro['data-fim'];
                        if (isset($get)):
                            header("Location: painel.php?exe=posts/pesquisa&Inicio={$datainicio}&Fim={$datafim}&user={$get}");
                        else:
                            header("Location: painel.php?exe=posts/pesquisa&Inicio={$datainicio}&Fim={$datafim}");
                        endif;

                    endif;
                    ?>
                    <form name="filtro" action="" method="post">
                        <input type="date" name = "data-inicio"/>
                        <input type="date" name = "data-fim" value = "" />
                        <input class = "btn" type = "submit" name = "filtrar" value = "Filtrar" />
                    </form>

                </div>
            </div>
            <a class="btn green" href="painel.php?exe=posts/create">Postar Canhoto para Usuário</a>
        </div>



        <h1 class="boxtitle">Canhotos:</h1>
        <div class="conteudo-tabela">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>NF-e</th>
                            <th>CT-e</th>
                            <th>Chave</th>
                            <th>Inserido em:</th>
                            <th>Cliente:</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <?php
                    $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
                    if ($empty):
                        WSErro("Oppsss: Você tentou editar um documento que não existe no sistema!", WS_INFOR);
                    endif;

                    // Começo do Action para ver se Ativa, desativa ou deleta o post
                    $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
                    if ($action):
                        require ('_models/AdminPosts.class.php');
                        $postAction = filter_input(INPUT_GET, 'post', FILTER_VALIDATE_INT);
                        $postUpdate = new AdminPosts;
                        switch ($action):
                            case 'active':
                                $postUpdate->ExeStatus($postAction, '1');
                                WSErro("O status do documento foi atualizado para <b>ativo</b>. Documento está disponível para download!", WS_ACCEPT);
                                break;
                            case 'inative':
                                $postUpdate->ExeStatus($postAction, '0');
                                WSErro("O status do documento foi atualizado para <b>inativo</b>. Ele não estará disponível para o usuário final!", WS_ALERT);
                                break;
                            case 'delete':
                                $postUpdate->ExeDelete($postAction);
                                WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                                break;
                            default :
                                WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
                        endswitch;
                    endif;
                    // Fim do Action para ver se Ativa, desativa ou deleta o post  

                    $readUser = new Read;
                    $readUser->ExeRead('users');
                    if ($readUser->getResult()):
                        $filtro = filter_input(INPUT_GET, 'filtro', FILTER_DEFAULT);
                        if (isset($filtro)):
                            $filtro = $filtro;
                            $query = "WHERE post_user =:cat ORDER BY post_status ASC, post_data DESC";
                        else:
                            $filtro = '';
                            $query = 'ORDER BY post_status ASC, post_data DESC';
                        endif;

                        $posti = 0;

                        $readPosts = new Read;
                        $readPosts->ExeRead("posts", "{$query}", "cat={$filtro}");
                        if ($readPosts->getResult()):
                            foreach ($readPosts->getResult() as $post):
                                $posti++;
                                extract($post);
                                $status = (!$post_status ? 'style="background: #fffed8"' : '');
                                ?>
                                <tbody>
                                    <tr>
                                        <td><?= $post_titulo ?></td>
                                        <td><?= $post_cte ?></td>
                                        <td><?= $post_chave ?></td>
                                        <td><?= date('d-m-y', strtotime($post_data)); ?></td>
                                        <td><?php
                    $user = new Read;
                    $user->ExeRead('users', "WHERE user_id = :id", "id={$post_user}");

                    echo $user->getResult()[0]['user_name'];
                                ?></td>
                                        <td>
                                            <ul class='acao'>
                                                <li><a href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar" class="action user_edit">Editar</a></li>
                                                <!--<li><a href="painel.php?exe=posts/index&post=<?= $post_id; ?>" title="Deletar" class="action user_dele">Deletar</a></li>-->
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>

            <?php
        endforeach;

    else:
//                    $Pager->ReturnPage();
        WSErro("Desculpe, Não existe nenhum documento cadastrado para este usuário!<br><br> Se essa for a página principal, selecione um usuário ao lado para poder exibir os documentos relacionados ao mesmo!", WS_INFOR);
    endif;
    ?>
                    </table>
                </div>
            </div>

            <div class="clear"></div>

    <?php
else:
    WSErro("Nenhum documento encontrado", WS_INFOR);
endif;
?>
    </section>
</section>   
</div>