<?php
if ($Link->getData()):
    
else:
    header("Location: " . HOME . DIRECTORY_SEPARATOR . '404');
endif;

?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                    <?php include (REQUIRE_PATH . '/inc/_sidebar_portifolio.php'); ?>
                </div>
            
            <div class="col-md-9 ">
            <div class="portifolio"> 
                <?php
                $cat_name = $Link->getData()['category_name'];
                $read = new Read;
                $read->ExeRead("categories", "WHERE category_name = :name", "name={$cat_name}");
                if (!$read->getResult()):
                    WSErro("No momento não temos nada cadastrado nesta categoria, volte mais tarde", WS_INFOR);
                else:
                    ?>   
                    <div>
                        <h2><?= $read->getResult()[0]['category_title'] ?></h2>
                        <br>
                        <h4><?= $read->getResult()[0]['category_content'] ?></h4>
                        <br><br><br>
                    </div>
                <?php
                endif;
                ?>
                
                <?php 
                $categoria = $read->getResult()[0]['category_id'];
                $readPort = new Read;
                $readPort->ExeRead("posts", "WHERE post_status = 1 AND (post_category = :cat || post_cat_parent = :cat) ORDER BY post_name ASC", "cat={$categoria}");
                if (!$readPort->getResult()):
                    WSErro("Infelismente não temos nenhum produto cadastrado, volte mais tarde", WS_INFOR);
                else:
                    $View = new View;
                    $portifolio_tpl = $View->Load('portifolio_geral');
                    
                    
                    foreach ($readPort->getResult() as $portifolio):
                        $portifolio['post_content'] = Check::Words($portifolio['post_content'], 8);
                        $View->Show($portifolio, $portifolio_tpl);
                        
                    endforeach;
                endif;
                
                ?>
                
                </div> <!-- Fim da Linha de 12colunas-->
            </div>
        </div>
    </div>
</section>

