<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="form_create">
    <h1 class="boxtitle">Cadastrar Canhoto de NF-e</h1>

    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if (isset($post) && $post['SendPostForm']):
        $post['post_status'] = ($post['SendPostForm'] == 'Cadastrar' ? '0' : '1' );
        $post['post_img'] = ( $_FILES['post_img']['tmp_name'] ? $_FILES['post_img'] : null );
        $post['post_data'] = date('Y/m/d');
        $post['post_categoria'] = 11;
        $post['post_cat_parent'] = 10;
        unset($post['SendPostForm']);

        echo "<pre>";
        print_r($post);
        echo "</pre>";
        
        require('_models/AdminPosts.class.php');
        $cadastra = new AdminPosts;
        $cadastra->ExeCreate($post);

        if ($cadastra->getResult()):
            echo "<script>
                    alert('O canhoto foi inserido com sucesso!');
                    window.location.replace(\"painel.php?exe=posts/index\");
                    </script>";
        else:
            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
        endif;
    endif;
    ?>


    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <label class="label">
            <span class="field">Imagem do Canhoto</span>
            <input type="file" name="post_img" />
        </label>

        <div class="label_line">

            <label class="label_small">
                <span class="field">Número da NF-e:</span>
                <input type="text" name="post_titulo" value="<?php if (isset($post['post_titulo'])) echo $post['post_titulo']; ?>" />
            </label>

            <label class="label_small">
                <span class="field">Série:</span>
                <input type="text" name="post_tipo" value="<?php if (isset($post['post_tipo'])) echo $post['post_tipo']; ?>" />
            </label>

            <label class="label_small">
                <span class="field">Número CT-e:</span>
                <input type="text" name="post_cte" value="<?php if (isset($post['post_cte'])) echo $post['post_cte']; ?>" />
            </label>
        </div>

        <label class="label">
            <span class="field">Chave do CT-e</span>
            <input type="text" name="post_chave" value="<?php if (isset($post['post_chave'])) echo $post['post_chave']; ?>" />
        </label>

        <div class="label_line">

<!--            <label class="label_small">
                <span class="field">Data da Criação:</span>
                <input type="datetime" class="formDate center" name="post_data" value="<?php
                if (isset($post['post_data'])): echo $post['post_data'];
                else: echo date('d/m/Y H:i:s');
                endif;
                ?>" />
            </label>-->

<!--            <label class="label_small">
                <span class="field">Categoria:</span>
                <select name="post_categoria">
                                        <option value=""> Selecione a categoria: </option>                        
                    <?php
                    $readSes = new Read;
                    $readSes->ExeRead("categorias", "WHERE categoria_parent IS NULL AND categoria_nome = :doc ORDER BY categoria_titulo ASC", "doc=documentos");
                    if ($readSes->getRowCount() >= 1):
                        foreach ($readSes->getResult() as $ses):
//                            echo "<option disabled=\"disabled\" value=\"\"> {$ses['categoria_titulo']} </option>";
                            $readCat = new Read;
                            $readCat->ExeRead("categorias", "WHERE categoria_parent = :parent ORDER BY categoria_titulo ASC", "parent={$ses['categoria_id']}");

                            if ($readCat->getRowCount() >= 1):
                                foreach ($readCat->getResult() as $cat):
                                    echo "<option ";

                                    if ($post['post_categoria'] == $cat['categoria_id']):
                                        echo "selected=\"selected\" ";
                                    endif;

                                    echo "value=\"{$cat['categoria_id']}\"> {$cat['categoria_titulo']} </option>";
                                endforeach;
                            endif;

                        endforeach;
                    endif;
                    ?>
                </select>
            </label>-->

            <label class="label_small">
                <span class="field">Cliente:</span>
                <select name="post_user">
                    <option value=""> Selecione um cliente: </option>                        
                    <!--<option value="<?= $_SESSION['userlogin']['user_id']; ?>"> <?= "{$_SESSION['userlogin']['user_name']} {$_SESSION['userlogin']['user_lastname']}"; ?> </option>-->
                    <?php
                    $readAut = new Read;
                    $readAut->ExeRead("users", "WHERE user_level = 2 ORDER BY user_name ASC");

                    if ($readAut->getRowCount() >= 1):
                        foreach ($readAut->getResult() as $aut):
                            echo "<option ";

                            if ($post['post_user'] == $aut['user_id']):
                                echo "selected=\"selected\" ";
                            endif;

                            echo "value=\"{$aut['user_id']}\"> {$aut['user_name']} {$aut['user_lastname']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

        </div><!--/line-->

        <div class="label_line">

            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Publicar" name="SendPostForm" />
        </div>
    </form>
</div> <!-- content home -->