<?php

require '../Fpdf/fpdf.php';
$inicio = filter_input(INPUT_GET, 'Inicio', FILTER_DEFAULT);
$fim = filter_input(INPUT_GET, 'Fim', FILTER_DEFAULT);
$usuario = filter_input(INPUT_GET, 'user', FILTER_DEFAULT);

if (isset($inicio)):
    $datainicio = explode('/', $inicio);
else:
    $datainicio = array();
endif;

if (isset($fim)):
    $datafim = explode('/', $fim);
else:
    $datafim = array();
endif;

$inicio = date('d-m', strtotime($datainicio[0]));
$fim = date('d-m', strtotime($datafim[0]));

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont("Arial", 'B', 16);
$pdf->Cell(190, 10, utf8_decode("Relatório por período: de {$inicio} até {$fim}"), 0, 0, "C");
$pdf->Ln();

$pdf->SetFont("Arial", "I", 10);
$pdf->Cell(20, 7, "NF-e", 1, 0, "C");
$pdf->Cell(20, 7, "CT-e", 1, 0, "C");
$pdf->Cell(80, 7, "Chave", 1, 0, "C");
$pdf->Cell(20, 7, "Data", 1, 0, "C");
$pdf->Cell(50, 7, "Cliente", 1, 0, "C");
$pdf->Ln();

$readPesquisa = new Read;
if (isset($usuario)):
    $readPesquisa->ExeRead("posts", "WHERE post_user = :id AND post_data BETWEEN :inicio AND :fim ORDER BY post_user ASC, post_data ASC", "id={$usuario}&inicio={$datainicio[0]}&fim={$datafim[0]}");
else:
    $readPesquisa->ExeRead("posts", "WHERE post_data BETWEEN :inicio AND :fim ORDER BY post_user ASC, post_data ASC", "inicio={$datainicio[0]}&fim={$datafim[0]}");
endif;
//$readPesquisa->ExeRead("posts", "WHERE post_data BETWEEN :inicio AND :fim ORDER BY post_user ASC, post_data ASC", "inicio={$datainicio[0]}&fim={$datafim[0]}");
if ($readPesquisa->getResult()):
    foreach ($readPesquisa->getResult() as $post):

        $pdf->Cell(20, 7, $post['post_titulo'], 1, 0, "C");
        $pdf->Cell(20, 7, $post['post_cte'], 1, 0, "C");
        $pdf->Cell(80, 7, $post['post_chave'], 1, 0, "L");
        $pdf->Cell(20, 7, date('d-m', strtotime($post['post_data'])), 1, 0, "C");
        $readUser = new Read;
        $readUser->ExeRead("users", "WHERE user_id = :id", "id={$post['post_user']}");
        if ($readUser->getResult()):
            $pdf->Cell(50, 7, utf8_decode($readUser->getResult()[0]['user_name']), 1, 0, "L");
        endif;
        $pdf->Ln();

    endforeach;
endif;
//ob_start();
ob_end_clean();
$pdf->Output();
?>