<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content list_emp">
    <aside>
        <h1 class="boxtitle">Informações Contato:</h1>
        

    </aside>
    <section class="content_statistics">

        

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Oppsss: Você tentou editar um post que não existe no sistema!", WS_INFOR);
        endif;


        $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
        if ($action):
            require ('_models/AdminContato.class.php');

            $postAction = filter_input(INPUT_GET, 'empid', FILTER_VALIDATE_INT);
            $postUpdate = new AdminContato;

            switch ($action):
                case 'active':
                    $postUpdate->ExeStatus($postAction, '1');
                    WSErro("O status do post foi atualizado para <b>ativo</b>. Post publicado!", WS_ACCEPT);
                    break;

                case 'inative':
                    $postUpdate->ExeStatus($postAction, '0');
                    WSErro("O status do post foi atualizado para <b>inativo</b>. Post agora é um rascunho!", WS_ALERT);
                    break;

                case 'delete':
                    $postUpdate->ExeDelete($postAction);
                    WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                    break;

                default :
                    WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
            endswitch;
        endif;


        $posti = 0;
        $readPosts = new Read;
        $readPosts->ExeRead("contato", "WHERE contato_id = 1");
        if ($readPosts->getResult()):
            foreach ($readPosts->getResult() as $emp):
                $posti++;
                extract($emp);
                $status = (!$contato_status ? 'style="background: #fffed8"' : '');
                ?>
                                       <!--  <article<?php if ($posti % 2 == 0) echo ' class="right"'; ?> <?= $status; ?>>-->
                <article style="width: 100%">
                    <div style="float: left;">
                        <h2 style="color: #990033; font-size: 24px;"><?= $contato_titulo ?></h2>
                        <p><span><b>Endereço - </b></span><?= Check::Words($contato_endereco, 10) ?></p>
                        <p><span><b>Bairro   - </b></span><?= $contato_bairro ?></p>
                        <p><span><b>Cidade   - </b></span><?= $contato_cidade ?></p>
                        <p><span><b>Estado   - </b></span><?= $contato_estado ?></p>
                        <p><span><b>Telefone - </b></span><?= $contato_telefone ?></p>
                        <p><span><b>Celular  - </b></span><?= $contato_celular ?></p>
                        <p><span><b>E-mail   - </b></span><?= $contato_email ?></p>
                        

                    </div>
                    <div style="float: right;">
                        <ul class="info post_actions">


                            <li><a class="act_edit" href="painel.php?exe=contato/update&empid=<?= $contato_id; ?>" title="Editar">Editar</a></li>

                            <?php if (!$contato_status): ?>
                                <li><a class="act_inative" href="painel.php?exe=contato/index&empid=<?= $contato_id; ?>&action=active" title="Ativar">Ativar</a></li>
                            <?php else: ?>
                                <li><a class="act_ative" href="painel.php?exe=contato/index&empid=<?= $contato_id; ?>&action=inative" title="Inativar">Inativar</a></li>
                            <?php endif; ?>

                                    <!-- <li><a class="act_delete" href="painel.php?exe=contato/index&empid=<?= $contato_id; ?>&action=delete" title="Excluir">Deletar</a></li>-->
                        </ul>
                    </div>

                </article>
                <?php
            endforeach;

        else:
            WSErro("Desculpe, ainda não existem posts cadastrados!", WS_INFOR);
        endif;
        ?>

        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->