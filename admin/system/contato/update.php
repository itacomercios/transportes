<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <article>

        <header>
            <h1>Atualizar Informações de Contato:</h1>
        </header>

        <?php
        $contato = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $contatoid = filter_input(INPUT_GET, 'empid', FILTER_VALIDATE_INT);

        if (isset($contato) && $contato['SendPostForm']):
            $contato['contato_status'] = ($contato['SendPostForm'] == 'Atualizar' ? '1' : '0' );
            $contato['contato_img'] = ( $_FILES['contato_img']['tmp_name'] ? $_FILES['contato_img'] : null );
            unset($contato['SendPostForm']);

            require('_models/AdminContato.class.php');
            $update = new AdminContato;
            $update->ExeUpdate($contatoid, $contato);


            WSErro($update->getError()[0], $update->getError()[1]);
        else:
            $read = new Read;
            $read->ExeRead("contato", "WHERE contato_id = :id", "id={$contatoid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=contato/index');
            else:
                $contato = $read->getResult()[0];
            endif;

        endif;
        $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
        if ($checkCreate && empty($update)):
            WSErro("O post <b>{$contato['contato_titulo']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT);
        endif;
        ?>

        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <label class="label">
                <span class="field">Título do Contato:</span>
                <input type="text" name="contato_titulo" value="<?php if (isset($contato['contato_titulo'])) echo $contato['contato_titulo']; ?>" />
            </label>

            <label class="label">
                <span class="field">Foto Principal:</span>
                <input type="file" name="contato_img" />
            </label>

            <label class="label">
                <span class="field">Endereço</span>
                <input type="text" name="contato_endereco" value="<?php if (isset($contato['contato_endereco'])) echo $contato['contato_endereco']; ?>" />
            </label>

            <div class="label_line">

                <label class="label_small">
                    <span class="field">Bairro:</span>
                    <input type="text" name="contato_bairro" value="<?php if (isset($contato['contato_bairro'])) echo $contato['contato_bairro']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Cidade:</span>
                    <input type="text" name="contato_cidade" value="<?php if (isset($contato['contato_cidade'])) echo $contato['contato_cidade']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Estado:</span>
                    <input type="text" name="contato_estado" value="<?php if (isset($contato['contato_estado'])) echo $contato['contato_estado']; ?>" />
                </label>

            </div><!--/line-->

            <div class="label_line">

                <label class="label_small">
                    <span class="field">Telefone:</span>
                    <input type="text" name="contato_telefone" value="<?php if (isset($contato['contato_telefone'])) echo $contato['contato_telefone']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Celular:</span>
                    <input type="text" name="contato_celular" value="<?php if (isset($contato['contato_celular'])) echo $contato['contato_celular']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">E-mail:</span>
                    <input type="text" name="contato_email" value="<?php if (isset($contato['contato_email'])) echo $contato['contato_email']; ?>" />
                </label>

            </div><!--/line-->

            <label class="label">
                <span class="field">Mapa do Google ou Foto do Local:</span>
                <textarea class="js_editor" name="contato_conteudo" rows="10"><?php if (isset($contato['contato_conteudo'])) echo htmlspecialchars($contato['contato_conteudo']); ?></textarea>
            </label>

            <input type="submit" class="btn green" value="Atualizar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->