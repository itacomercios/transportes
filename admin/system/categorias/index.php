<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="cat_list">
    <section class="lista_geral">
        <a class="btn blue" href="painel.php?exe=categorias/create">Cadastrar Nova Categoria</a>

        <h1 class="boxtitle">Categorias:</h1>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar uma categoria que não existe no sistema!", WS_INFOR);
        endif;

        $delCat = filter_input(INPUT_GET, 'delete', FILTER_VALIDATE_INT);
        if ($delCat):
            require ('_models/AdminCategoria.class.php');
            $deletar = new AdminCategoria;
            $deletar->ExeDelete($delCat);

            WSErro($deletar->getError()[0], $deletar->getError()[1]);
        endif;


        $readSes = new Read;
        $readSes->ExeRead("categorias", "WHERE categoria_parent IS NULL ORDER BY categoria_titulo ASC");
        if (!$readSes->getResult()):

        else:
            foreach ($readSes->getResult() as $ses):
                extract($ses);

                $readPosts = new Read;
                $readPosts->ExeRead("posts", "WHERE post_cat_parent = :parent", "parent={$categoria_id}");

                $readCats = new Read;
                $readCats->ExeRead("categorias", "WHERE categoria_parent = :parent", "parent={$categoria_id}");

                $countSesPosts = $readPosts->getRowCount();
                $countSesCats = $readCats->getRowCount();
                ?>
                <section class="container_categorias">

                    <header class="detalhes">
                        <h1><?= $categoria_titulo; ?>  <span>( <?= $countSesPosts; ?> posts ) ( <?= $countSesCats; ?> Categorias )</span></h1>
                        <div class="sub_titulo">
                            <p class="tagline"><?= $categoria_conteudo; ?></p>

                            <div class="post_actions" >
                                <a class="act_edit" href="painel.php?exe=categorias/update&catid=<?= $categoria_id; ?>" title="Editar">Editar</a>
                                <a class="act_delete" href="painel.php?exe=categorias/index&delete=<?= $categoria_id; ?>" title="Excluir">Deletar</a>
                            </div>
                        </div>
                    </header>
                    <section class="container_sub">
                        <h4>Sub categorias de <b><?= $categoria_titulo ?></b></h4>

                        <?php
                        $readSub = new Read;
                        $readSub->ExeRead("categorias", "WHERE categoria_parent = :subparent", "subparent={$categoria_id}");
                        if (!$readSub->getResult()):

                        else:
                            $a = 0;
                            foreach ($readSub->getResult() as $sub):
                                $a++;

                                $readCatPosts = new Read;
                                $readCatPosts->ExeRead("posts", "WHERE post_categoria = :categoryid", "categoryid={$sub['categoria_id']}");
                                ?>
                                <article class="icones">
                                    <h4><a target="_blank" href="../categoria/<?= $sub['categoria_nome']; ?>" title="Ver Categoria"><?= $sub['categoria_titulo']; ?></a>  ( <?= $readCatPosts->getRowCount(); ?> posts )</h4>

                                    <div class="post_actions">
                                        <!-- <li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($sub['categoria_date'])); ?>Hs</li>-->
                                        <!-- <li><a class="act_view" target="_blank" href="../produtos/<?= $sub['categoria_nome']; ?>" title="Ver no site">Ver no site</a></li>-->
                                        <a class="act_edit" href="painel.php?exe=categorias/update&catid=<?= $sub['categoria_id']; ?>" title="Editar">Editar</a>
                                        <a class="act_delete" href="painel.php?exe=categorias/index&delete=<?= $sub['categoria_id']; ?>" title="Excluir">Deletar</a>
                                    </div>
                                </article>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </section>
                </section>
                <?php
            endforeach;
        endif;
        ?>
    </section>
</div> <!-- content home -->