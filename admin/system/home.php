<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="home">
    <aside>
        <h1 class="boxtitle">Estatísticas:</h1>
        <article class="sitecontent boxaside">
            <h1 class="boxsubtitle">Conteúdo:</h1>
            <?php
            //OBJETO READ
            $read = new Read;

            //VISITAS DO SITE
            $read->FullRead("SELECT SUM(siteviews_views) AS views FROM siteviews");
            $Views = $read->getResult()[0]['views'];

            //USUÁRIOS
            $read->FullRead("SELECT SUM(siteviews_users) AS users FROM siteviews");
            $Users = $read->getResult()[0]['users'];

            //MÉDIA DE PAGEVIEWS
            $read->FullRead("SELECT SUM(siteviews_pages) AS pages FROM siteviews");
            $ResPages = $read->getResult()[0]['pages'];
            $Pages = substr($ResPages / $Users, 0, 5);

            //POSTS
            $read->ExeRead("posts");
            $Posts = $read->getRowCount();

            // USERS
            $readUser = new Read;
            $readUser->ExeRead('users', "WHERE user_level = 2");
            $Users = $readUser->getRowCount();
            ?>

            <ul>
                <li class="view"><span><?= $Views; ?></span> visitas</li>
                <li class="line"></li>
                <li class="post"><span><?= $Users; ?></span> usuários cadastrados</li>
                <li class="line"></li>
                <li class="post"><span><?= $Posts; ?></span> canhotos cadastrados</li>

            </ul>

        </article>

        <article class="useragent boxaside">
            <h1 class="boxsubtitle">Navegador:</h1>

            <?php
            //LE O TOTAL DE VISITAS DOS NAVEGADORES
            $read->FullRead("SELECT SUM(agent_views) AS TotalViews FROM siteviews_agent");
            $TotalViews = $read->getResult()[0]['TotalViews'];

            $read->ExeRead("siteviews_agent", "ORDER BY agent_views DESC LIMIT 3");
            if (!$read->getResult()):
                WSErro("Oppsss, Ainda não existem estatísticas de navegadores!", WS_INFOR);
            else:
                echo "<ul>";
                foreach ($read->getResult() as $nav):
                    extract($nav);

                    //REALIZA PORCENTAGEM DE VISITAS POR NAVEGADOR!
                    $percent = substr(( $agent_views / $TotalViews ) * 100, 0, 5);
                    ?>
                    <li>
                        <p><strong><?= $agent_name; ?>:</strong> <?= $percent; ?>%</p>
                        <span style="width: <?= $percent; ?>%"></span>
                        <p><?= $agent_views; ?> visitas</p>
                    </li>
                    <?php
                endforeach;
                echo "</ul>";
            endif;
            ?>

            <div class="clear"></div>
        </article>
    </aside>

    <section class="content_statistics">
        <h1 class="boxtitle">Publicações:</h1>
        <a class="btn blue" href="painel.php?exe=posts/create">Postar Novo Canhoto</a>
        <section>
            <h1 class="boxsubtitle">Canhotos adicionados Recentemente:</h1>
            <?php
            $read->ExeRead("posts", "WHERE post_status = 1 ORDER BY post_data DESC LIMIT 10");
            if ($read->getResult()):
                ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>NF-e</th>
                                <th>CT-e</th>
                                <th>Chave</th>
                                <!--<th>Data</th>-->
                                <th>Empresa</th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($read->getResult() as $re):
                            extract($re);
                            ?>
                            <tbody>
                                <tr>
                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <!--<td><?= date('d/m/Y', strtotime($post_data)); ?></td>-->
                                    <td>
                                        <?php
                                        $empresa = new Read;
                                        $empresa->ExeRead("users", "WHERE user_id = :id", "id={$post_user}");
                                        
                                        echo $empresa->getResult()[0]['user_name'];
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                            
                            <?php
                        endforeach;
                        ?>
                            </table>
                </div>
            <?php
                    endif;
                    ?>
                    </section>          

                    </section> <!-- Estatísticas -->
                    <div class="clear"></div>
            </div> <!-- content home -->