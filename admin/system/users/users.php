<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="form_create">
    <div class="user_menu">
        <h1 class="boxtitle">Usuários/ Clientes:</h1>
        <a href="painel.php?exe=users/create" title="Cadastrar Novo" class="btn green">Cadastrar Usuário / Cliente</a>
    </div>
    <?php
    $delete = filter_input(INPUT_GET, 'delete', FILTER_VALIDATE_INT);
    if ($delete):
        require('_models/AdminUser.class.php');
        $delUser = new AdminUser;
        $delUser->ExeDelete($delete);
        WSErro($delUser->getError()[0], $delUser->getError()[1]);
    endif;
    ?>

    <div class="conteudo-tabela">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Registro</th>
                        <th>Atualização</th>
                        <th>Nível</th>
                        <th>Ação</th>
                    </tr>
                </thead>

                <?php
                $read = new Read;
                $read->ExeRead("users", "ORDER BY user_level DESC, user_name ASC");
                if ($read->getResult()):
                    foreach ($read->getResult() as $user):
                        extract($user);
                        $user_lastupdate = ($user_lastupdate ? date('d/m/Y H:i', strtotime($user_lastupdate)) . ' hs' : '-');
                        $nivel = ['', 'User', 'Cliente', 'Administrador'];
                        ?>            
                        <tbody>
                            <tr>
                                <td><?= $user_id ?></td>
                                <td><?= $user_name . ' ' . $user_lastname; ?></td>
                                <td><?= $user_email; ?></td>
                                <td><?= date('d/m/Y', strtotime($user_registration)); ?></td>
                                <td><?= $user_lastupdate; ?></td>
                                <td><?= $nivel[$user_level]; ?></td>
                                <td>
                                    <ul class='acao'>
                                        <li><a href="painel.php?exe=users/update&userid=<?= $user_id; ?>" title="Editar" class="action user_edit">Editar</a></li>
                                        <li><a href="painel.php?exe=users/users&delete=<?= $user_id; ?>" title="Deletar" class="action user_dele">Deletar</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                        <?php
                    endforeach;
                endif;
                ?>
            </table>
        </div>
    </div>