<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <article>

        <header>
            <h1>Criar Texto de Informações para usuário:</h1>
        </header>

        <?php
        $emp = filter_input_array(INPUT_POST, FILTER_DEFAULT);

        if (isset($emp) && $emp['SendPostForm']):
            $emp['info_status'] = ($emp['SendPostForm'] == 'Cadastrar' ? '0' : '1' );
            unset($emp['SendPostForm']);

            require('_models/AdminInfo.class.php');
            $cadastra = new AdminInfo();
            $cadastra->ExeCreate($emp);

            if ($cadastra->getResult()):
                echo "<script>
                    alert('A informação foi incluida com sucesso!');
                    window.location.replace(\"painel.php?exe=info/index\");
                    </script>";
            else:
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            endif;
        endif;
        ?>


        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <div class="label_line">
                <label class="label_small">
                    <span class="field">Titulo:</span>
                    <input type="text" name="info_titulo" value="<?php if (isset($emp['info_titulo'])) echo $emp['info_titulo']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Sub Título:</span>
                    <input type="text" name="info_subtitulo" value="<?php if (isset($emp['info_subtitulo'])) echo $emp['info_subtitulo']; ?>" />
                </label>
            </div>

            <label class="label">
                <span class="field">Conteúdo:</span>
                <textarea class="js_editor" name="info_conteudo" rows="6"><?php if (isset($emp['info_conteudo'])) echo htmlspecialchars($emp['info_conteudo']); ?></textarea>
            </label>
            
            <label class="label">
                <span class="field">Dica:</span>
                <textarea class="js_editor" name="info_dica" rows="6"><?php if (isset($emp['info_dica'])) echo htmlspecialchars($emp['info_dica']); ?></textarea>
            </label>


            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Cadastrar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->