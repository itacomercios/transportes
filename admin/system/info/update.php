<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <article>

        <header>
            <h1>Atualizar Texto Referente a Empresa:</h1>
        </header>

        <?php
        $emp = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $empid = filter_input(INPUT_GET, 'empid', FILTER_VALIDATE_INT);

        if (isset($emp) && $emp['SendPostForm']):
            $emp['info_status'] = ($emp['SendPostForm'] == 'Atualizar' ? '0' : '1' );
            unset($emp['SendPostForm']);

            require('_models/AdminInfo.class.php');
            $update = new AdminInfo;
            $update->ExeUpdate($empid, $emp);

            if ($update->getResult()):
                echo "<script>
                    alert('A informação foi atualizado com sucesso!');
                    window.location.replace(\"painel.php?exe=info/index\");
                    </script>";
            endif;
        else:
            $read = new Read;
            $read->ExeRead("informacoes", "WHERE info_id = :id", "id={$empid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=info/index&empty=true');
            else:
                $emp = $read->getResult()[0];
            endif;

        endif;

        $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
        if ($checkCreate && empty($update)):
            WSErro("O post <b>{$emp['info_titulo']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT);
        endif;
        ?>


        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <div class="label_line">
                <label class="label_small">
                    <span class="field">Titulo:</span>
                    <input type="text" name="info_titulo" value="<?php if (isset($emp['info_titulo'])) echo $emp['info_titulo']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Sub Título:</span>
                    <input type="text" name="info_subtitulo" value="<?php if (isset($emp['info_subtitulo'])) echo $emp['info_subtitulo']; ?>" />
                </label>
            </div>

             <label class="label">
                <span class="field">Conteúdo:</span>
                <textarea class="js_editor" name="info_conteudo" rows="6"><?php if (isset($emp['info_conteudo'])) echo htmlspecialchars($emp['info_conteudo']); ?></textarea>
            </label>
            
            <label class="label">
                <span class="field">Dica:</span>
                <textarea class="js_editor" name="info_dica" rows="6"><?php if (isset($emp['info_dica'])) echo htmlspecialchars($emp['info_dica']); ?></textarea>
            </label>


            <!--<input type="submit" class="btn blue" value="Atualizar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Atualizar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->