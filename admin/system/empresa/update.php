<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <article>

        <header>
            <h1>Atualizar Texto Referente a Empresa:</h1>
        </header>

        <?php
        $emp = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $empid = filter_input(INPUT_GET, 'empid', FILTER_VALIDATE_INT);

        if (isset($emp) && $emp['SendPostForm']):
            $emp['emp_status'] = ($emp['SendPostForm'] == 'Atualizar' ? '0' : '1' );
            $emp['emp_img'] = ( $_FILES['emp_img']['tmp_name'] ? $_FILES['emp_img'] : 'null' );
            unset($emp['SendPostForm']);

            require('_models/AdminEmpresa.class.php');
            $update = new AdminEmpresa;
            $update->ExeUpdate($empid, $emp);

            if ($update->getResult()):
                echo "<script>
                    alert('A informação foi atualizada com sucesso!');
                    window.location.replace(\"painel.php?exe=empresa/index\");
                    </script>";
            endif;
        else:
            $read = new Read;
            $read->ExeRead("empresa", "WHERE emp_id = :id", "id={$empid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=empresa/index&empty=true');
            else:
                $emp = $read->getResult()[0];
            endif;

        endif;

        if (!empty($_SESSION['errCapa'])):
            WSErro($_SESSION['errCapa'], E_USER_WARNING);
            unset($_SESSION['errCapa']);
        endif;

        $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
        if ($checkCreate && empty($update)):
            WSErro("O post <b>{$emp['emp_titulo']}</b> foi cadastrado com sucesso no sistema!", WS_ACCEPT);
        endif;
        ?>


        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <label class="label">
                <span class="field">Foto Principal:</span>
                <input type="file" name="emp_img" />
            </label>

            <div class="label_line">
                <label class="label_small">
                    <span class="field">Titulo:</span>
                    <input type="text" name="emp_titulo" value="<?php if (isset($emp['emp_titulo'])) echo $emp['emp_titulo']; ?>" />
                </label>

                <label class="label_small">
                    <span class="field">Sub Título:</span>
                    <input type="text" name="emp_subtitulo" value="<?php if (isset($emp['emp_subtitulo'])) echo $emp['emp_subtitulo']; ?>" />
                </label>
            </div>

            <label class="label">
                <span class="field">Conteúdo:</span>
                <textarea class="js_editor" name="emp_conteudo" rows="10"><?php if (isset($emp['emp_conteudo'])) echo htmlspecialchars($emp['emp_conteudo']); ?></textarea>
            </label>


            <!--<input type="submit" class="btn blue" value="Atualizar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Atualizar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->