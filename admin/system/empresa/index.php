<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>

    <section class="conteudo_lista">
        <a class="btn blue" href="painel.php?exe=empresa/create">Cadastrar um novo Texto</a>
        <h1 class="boxtitle">Publicações:</h1>
        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Oppsss: Você tentou editar um post que não existe no sistema!", WS_INFOR);
        endif;

        $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
        if ($action):
            require ('_models/AdminEmpresa.class.php');

            $postAction = filter_input(INPUT_GET, 'empid', FILTER_VALIDATE_INT);
            $postUpdate = new AdminEmpresa;

            switch ($action):
                case 'active':
                    $postUpdate->ExeStatus($postAction, '1');
                    WSErro("O status do post foi atualizado para <b>ativo</b>. Post publicado!", WS_ACCEPT);
                    break;

                case 'inative':
                    $postUpdate->ExeStatus($postAction, '0');
                    WSErro("O status do post foi atualizado para <b>inativo</b>. Post agora é um rascunho!", WS_ALERT);
                    break;

                case 'delete':
                    $postUpdate->ExeDelete($postAction);
                    WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                    break;

                default :
                    WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
            endswitch;
        endif;


        $posti = 0;
        $readPosts = new Read;
        $readPosts->ExeRead("empresa", "ORDER BY emp_status ASC, emp_nome ASC");
        if ($readPosts->getResult()):
            foreach ($readPosts->getResult() as $emp):
                $posti++;
                extract($emp);
                $status = (!$emp_status ? 'style="background: #fffed8"' : '');
                ?>
                                                       <!--  <article<?php if ($posti % 2 == 0) echo ' class="right"'; ?> <?= $status; ?>>-->
                <div>
                    <article <?php echo ' class="detalhes"'; ?> <?= $status; ?>>
                        <div class="img">
                            <img height="30" src="<?= HOME . '/tim.php?src=' . HOME . '/uploads/' . $emp_img ?>">
                        </div>
                        <h4><?= $emp_titulo ?></h4>
                        <p><?= Check::Words($emp_conteudo, 10) ?></p>

                        <div class="post_actions">
                            <a class="act_edit" href="painel.php?exe=empresa/update&empid=<?= $emp_id; ?>" title="Editar">Editar</a>

                            <?php if (!$emp_status): ?>
                                <a class="act_inative" href="painel.php?exe=empresa/index&empid=<?= $emp_id; ?>&action=active" title="Ativar">Ativar</a>
                            <?php else: ?>
                                <a class="act_ative" href="painel.php?exe=empresa/index&empid=<?= $emp_id; ?>&action=inative" title="Inativar">Inativar</a>
                            <?php endif; ?>
                                <a class="act_delete" href="painel.php?exe=empresa/index&empid=<?= $emp_id; ?>&action=delete" title="Excluir">Deletar</a>
                        </div>
                    </article>
                </div>        
                <?php
            endforeach;

        else:
            WSErro("Desculpe, ainda não existem posts cadastrados!", WS_INFOR);
        endif;
        ?>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->