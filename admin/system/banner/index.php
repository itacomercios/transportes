<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;

//OBJETO READ
?>
<div class="content home">

    <section class="content_statistics">
        <a class="btn btn_verde" href="painel.php?exe=banner/create">Cadastrar Novo Banner</a>
        <br><br>
        <h1 class="boxtitle">Publicações:</h1>
        <section>
            <?php
            $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
            if ($empty):
                WSErro("Oppsss: Você tentou editar um banner que não existe no sistema!", WS_INFOR);
            endif;

            // Começo do Action para ver se Ativa, desativa ou deleta o post
            $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
            if ($action):
                require ('_models/AdminBanner.class.php');
                $postAction = filter_input(INPUT_GET, 'post', FILTER_VALIDATE_INT);
                $postUpdate = new AdminBanner;
                switch ($action):
                    case 'active':
                        $postUpdate->ExeStatus($postAction, '1');
                        WSErro("O status do banner foi atualizado para <b>ativo</b>. Banner publicado!", WS_ACCEPT);
                        break;
                    case 'inative':
                        $postUpdate->ExeStatus($postAction, '0');
                        WSErro("O status do Banner foi atualizado para <b>inativo</b>. Banner agora é um rascunho!", WS_ALERT);
                        break;
                    case 'delete':
                        $postUpdate->ExeDelete($postAction);
                        WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                        break;
                    default :
                        WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
                endswitch;
            endif;
            // Fim do Action para ver se Ativa, desativa ou deleta o post    


            $posti = 0;
            $readPosts = new Read;
            $readPosts->ExeRead("banner", "ORDER BY banner_status ASC, banner_nome ASC");
            if ($readPosts->getResult()):
                foreach ($readPosts->getResult() as $post):
                    $posti++;
                    extract($post);
                    $status = (!$banner_status ? 'style="background: #fffed8"' : '');
                    ?>
                    <article<?php echo ' class="detalhes"'; ?> <?= $status; ?>>

                        <div class="img">
                            <img height="30" src="<?= HOME . '/tim.php?src=' . HOME . '/uploads/' . $banner_img ?>">
                        </div>

                        <h4><?= Check::Words($banner_titulo, 10) ?></h4>
                        <span style="font-size: 12px; float: left;"><a target="_blank" href="http://<?= $banner_link ?>"><?= $banner_link ?></a></span>
                        <div class="post_actions">
                            <a class="act_edit" href="painel.php?exe=banner/update&postid=<?= $banner_id; ?>" title="Editar">Editar</a>
                            <?php if (!$banner_status): ?>
                                <a class="act_inative" href="painel.php?exe=banner/index&post=<?= $banner_id; ?>&action=active" title="Ativar">Ativar</a>
                            <?php else: ?>
                                <a class="act_ative" href="painel.php?exe=banner/index&post=<?= $banner_id; ?>&action=inative" title="Inativar">Inativar</a>
                            <?php endif; ?>
                            <a class="act_delete" href="painel.php?exe=banner/index&post=<?= $banner_id; ?>&action=delete" title="Excluir">Deletar</a>
                        </div>
                    </article>
                    <?php
                endforeach;
            else:
//                    $Pager->ReturnPage();
                WSErro("Desculpe, ainda não existem nenhum banner cadastrado!", WS_INFOR);
            endif;
            ?>
        </section>
    </section>   



    <div class="clear"></div>
</div> <!-- content home -->