<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <article>

        <header>
            <h1>Atualizar Post:</h1>
        </header>

        <?php
        $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $postid = filter_input(INPUT_GET, 'postid', FILTER_VALIDATE_INT);

        if (isset($post) && $post['SendPostForm']):
            $post['banner_status'] = ($post['SendPostForm'] == 'Atualizar' ? '0' : '1' );
            $post['banner_img'] = ( $_FILES['banner_img']['tmp_name'] ? $_FILES['banner_img'] : 'null' );
            unset($post['SendPostForm']);

            require('_models/AdminBanner.class.php');
            $cadastra = new AdminBanner;
            $cadastra->ExeUpdate($postid, $post);

            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            header('Location: painel.php?exe=banner/index');
        else:
            $read = new Read;
            $read->ExeRead("banner", "WHERE banner_id = :id", "id={$postid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=banner/index');
            else:
                $post = $read->getResult()[0];
//                header('Location: painel.php?exe=banner/index');
            endif;
        endif;

        if (!empty($_SESSION['errCapa'])):
            WSErro($_SESSION['errCapa'], E_USER_WARNING);
            unset($_SESSION['errCapa']);
        endif;

        $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
        if ($checkCreate && empty($cadastra)):
            WSErro("O Banner <b>{$post['banner_titulo']}</b> foi atualizado com sucesso no sistema!", WS_ACCEPT);
        endif;
        ?>


        <form name="PostForm" action="" method="post" enctype="multipart/form-data">

            <label class="label">
                <span class="field">IMAGEM:</span>
                <input type="file" name="banner_img" />
            </label>

            <label class="label">
                <span class="field">Título (o mesmo que aparece abaixo do banner)</span>
                <input type="text" name="banner_titulo" value="<?php if (isset($post['banner_titulo'])) echo $post['banner_titulo']; ?>" />
            </label>

            <label class="label">
                <span class="field">Link para redirecionamento</span>
                <input type="text" name="banner_link" value="<?php if (isset($post['banner_link'])) echo $post['banner_link']; ?>" />
            </label>

            <input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />
            <input type="submit" class="btn green" value="Cadastrar & Publicar" name="SendPostForm" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->