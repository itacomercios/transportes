<?php
ob_start();
require('_app/Config.inc.php');

$Session = new Session;

$estilo = filter_input(INPUT_GET, 'estilo', FILTER_DEFAULT);

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="mit" content="2017-09-13T07:32:46-03:00+50652">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--[if lt IE 9]>
            <script src="../../_cdn/html5.js"></script>
         <![endif]-->   

        <?php
        $Link = new Link;
        $Link->getTags();
        ?>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="icon" href="../img/favicon.png">

        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/css/bootstrap.min.css">
        
        <?php 
        if (isset($estilo)):
            ?>
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/estilo<?= $estilo ?>.css">
        <?php
        else:
            ?>
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/estilo1.css">
        <?php
        endif;
        ?>
        
        
        <link rel="stylesheet" href="<?= INCLUDE_PATH; ?>/css/reset.css">
        <link rel="stylesheet" href="<?= HOME; ?>/_cdn/shadowbox/shadowbox.css">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,700i" rel="stylesheet">

    </head>
    <body>
        <script type="text/javascript">
            document.documentElement.className += ' js';
        </script>

        <?php
        require(REQUIRE_PATH . '/inc/header.inc.php');

        if (!require($Link->getPatch())):
            WSErro('Erro ao incluir arquivo de navegação!', WS_ERROR, true);
        endif;

        require(REQUIRE_PATH . '/inc/footer.inc.php');
        ?> 


    </body>

    <script src="<?= HOME ?>/_cdn/jquery.js"></script>
    <script src="<?= HOME ?>/_cdn/jcycle.js"></script>
    <script src="<?= HOME ?>/_cdn/jmask.js"></script>
    <script src="<?= HOME ?>/_cdn/shadowbox/shadowbox.js"></script>
    <script src="<?= HOME ?>/_cdn/_plugins.conf.js"></script>
    <script src="<?= HOME ?>/_cdn/_scripts.conf.js"></script>
    <script src="<?= HOME ?>/_cdn/combo.js"></script>
    <script src="<?= HOME ?>/_cdn/app.js"></script>

    <script src="<?= INCLUDE_PATH ?>/css/js/jquery-3.3.1.js"></script>
    <script src="<?= INCLUDE_PATH ?>/css/js/slide.js"></script>

</html>

