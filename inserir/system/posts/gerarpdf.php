<?php

require '../Fpdf/fpdf.php';
$inicio = filter_input(INPUT_GET, 'Inicio', FILTER_DEFAULT);
$fim = filter_input(INPUT_GET, 'Fim', FILTER_DEFAULT);

if (isset($inicio)):
    $datainicio = explode('/', $inicio);
else:
    $datainicio = array();
endif;

if (isset($fim)):
    $datafim = explode('/', $fim);
else:
    $datafim = array();
endif;

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont("Arial", 'B', 16);
$pdf->Cell(190,10,utf8_decode("Relatório por período: de {$datainicio[0]} até {$datafim[0]}"),0,0,"C");
$pdf->Ln();

$pdf->SetFont("Arial","I",10);
$pdf->Cell(30,7,"NF-e",1,0,"C");
$pdf->Cell(30,7,"CT-e",1,0,"C");
$pdf->Cell(70,7,"Chave",1,0,"C");
$pdf->Cell(30,7,"Data",1,0,"C");
$pdf->Cell(30,7,"Cliente",1,0,"C");

$readPesquisa = new Read;
$readPesquisa->ExeRead("posts", "WHERE post_data BETWEEN :inicio AND :fim ORDER BY post_user ASC, post_data ASC", "inicio={$datainicio[0]}&fim={$datafim[0]}");
if ($readPesquisa->getResult()):
    foreach ($readPesquisa->getResult() as $post):

        $pdf->Cell(30,7,$post['post_titulo'],1,0,"C");
        $pdf->Cell(30,7,$post['post_cte'],1,0,"C");
        $pdf->Cell(70,7,$post['post_chave'],1,0,"C");
        $pdf->Cell(30,7,$post['post_data'],1,0,"C");
        $readUser = new Read;
        $readUser->ExeRead("users", "WHERE user_id = :id", "id={$post['post_user']}");
        if ($readUser->getResult()):
            $pdf->Cell(30,7,$readUser->getResult()[0]['user_name'],1,0,"C");
        endif;
        $pdf->Ln();

    endforeach;
endif;
//ob_start();
ob_end_clean();
$pdf->Output();
?>