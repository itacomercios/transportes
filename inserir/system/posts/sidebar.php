<?php
$getexe = filter_input(INPUT_GET, 'filtro', FILTER_DEFAULT);

//ATIVA MENU
if (isset($getexe)):
    $linkto = explode('/', $getexe);
else:
    $linkto = array();
endif;
?>
<article class="navegacao-user">
    <h1 class="boxtitle">Usuários</h1>
    <div>
        <?php
        $read = new Read;
        $read->ExeRead("users", "WHERE user_level = 2 ORDER BY user_name ASC");
        if (!$read->getResult()):
            WSErro("Opss, O menu não contém nenhum Usuário cadastrado até o momento", WS_INFOR);
        else:
            foreach ($read->getResult() as $user):
                extract($user);
                ?>
                <a class="<?php if (in_array($user_id, $linkto)) echo 'active'; ?>" href="<?= HOME . '/admin/painel.php?exe=posts/index&filtro=' . $user_id ?>"><?= $user_name ?></a>
                <?php
            endforeach;
        endif;
        ?>
    </div>
</article>