<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="box-documentos">
    <?php include "sidebar.php"; ?>

    <section class="conteudo_lista">
        <div class="conteudo_lista_menu">
            <a class="btn blue" href="painel.php?exe=posts/create">Postar Canhoto para Usuário</a>

            <div class="pesquisa">
                <?php
                $search = filter_input(INPUT_POST, 'search-texto', FILTER_DEFAULT);
                if (!empty($search)):
                    $search = strip_tags(trim(urlencode($search)));
                    header ('Location: painel.php?exe=posts/pesquisa&filtro=' . $search);
//                    header('Location: ' . HOME . '/pesquisa/' . $search);
                endif;
                ?>
                <form name="search" action="" method="post">
                    <input class = "fls" type = "text" name = "search-texto" placeholder="Procure pelo número da NF-e!"/>
                    <input class = "btn" type = "submit" name = "sendsearch" value = "" />
                </form>
            </div>
        </div>

        <h1 class="boxtitle">Canhotos:</h1>
        <section>
            <div class="titulo_da_coluna">
                <p>NF-e</p>
                <p>Série</p>
                <p>CT-e</p>
                <p>Chave</p>
                <!--<p>Downloads</p>-->
                <p>Data</p>
                <p>Ações</p>
            </div>
            <?php
            $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
            if ($empty):
                WSErro("Oppsss: Você tentou editar um documento que não existe no sistema!", WS_INFOR);
            endif;

            // Começo do Action para ver se Ativa, desativa ou deleta o post
            $action = filter_input(INPUT_GET, 'action', FILTER_DEFAULT);
            if ($action):
                require ('_models/AdminPosts.class.php');
                $postAction = filter_input(INPUT_GET, 'post', FILTER_VALIDATE_INT);
                $postUpdate = new AdminPosts;
                switch ($action):
                    case 'active':
                        $postUpdate->ExeStatus($postAction, '1');
                        WSErro("O status do documento foi atualizado para <b>ativo</b>. Documento está disponível para download!", WS_ACCEPT);
                        break;
                    case 'inative':
                        $postUpdate->ExeStatus($postAction, '0');
                        WSErro("O status do documento foi atualizado para <b>inativo</b>. Ele não estará disponível para o usuário final!", WS_ALERT);
                        break;
                    case 'delete':
                        $postUpdate->ExeDelete($postAction);
                        WSErro($postUpdate->getError()[0], $postUpdate->getError()[1]);
                        break;
                    default :
                        WSErro("Ação não foi identifica pelo sistema, favor utilize os botões!", WS_ALERT);
                endswitch;
            endif;
            // Fim do Action para ver se Ativa, desativa ou deleta o post  
            
            $readUser = new Read;
            $readUser->ExeRead('users');
            if ($readUser->getResult()):
                $filtro = filter_input(INPUT_GET, 'filtro', FILTER_DEFAULT);
                if (isset($filtro)):
                    $filtro = $filtro;
                    $query = "WHERE post_user =:cat ORDER BY post_status ASC, post_data DESC";
                else:
                    $filtro = '';
                    $query = 'ORDER BY post_status ASC, post_data DESC';
                endif;

                $posti = 0;

                $readPosts = new Read;
                $readPosts->ExeRead("posts", "{$query}", "cat={$filtro}");
                if ($readPosts->getResult()):
                    foreach ($readPosts->getResult() as $post):
                        $posti++;
                        extract($post);
                        $status = (!$post_status ? 'style="background: #fffed8"' : '');
                        ?>
                        <div <?php echo ' class="detalhes"'; ?> <?= $status; ?>>

                            <p><?= Check::Words($post_titulo, 10) ?></p>
                            <p><?= $post_tipo ?></p>
                            <p><?= $post_cte ?></p>
                            <p><?= $post_chave ?></p>
                            <p><?= date('d/m/Y', strtotime($post_data))  ?></p>

                            <div class="post_actions">
                                <a class="act_edit" href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar">Editar</a>

                                <?php if (!$post_status): ?>
                                    <a class="act_inative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=active" title="Ativar">Ativar</a>
                                <?php else: ?>
                                    <a class="act_ative" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=inative" title="Inativar">Inativar</a>
                                <?php endif; ?>

                                <a class="act_delete" href="painel.php?exe=posts/index&post=<?= $post_id; ?>&action=delete" title="Excluir">Deletar</a>
                            </div>
                        </div>
                        <?php
                    endforeach;

                else:
//                    $Pager->ReturnPage();
                    WSErro("Desculpe, Não existe nenhum documento cadastrado para este usuário!<br><br> Se essa for a página principal, selecione um usuário ao lado para poder exibir os documentos relacionados ao mesmo!", WS_INFOR);
                endif;
                ?>

                <div class="clear"></div>

                <?php
            else:
                WSErro("Nenhum documento encontrado", WS_INFOR);
            endif;
            ?>
        </section>
    </section>   
</div>