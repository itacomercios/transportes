<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="form_create">
    <h1 class="boxtitle">Cadastrar Canhoto de NF-e</h1>

    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    if (isset($post) && $post['SendPostForm']):
        $post['post_status'] = ($post['SendPostForm'] == 'Cadastrar' ? '0' : '1' );
        $post['post_img'] = ( $_FILES['post_img']['tmp_name'] ? $_FILES['post_img'] : null );
        $post['post_cat_parent'] = 10;
        $post['post_categoria'] = 11;
        $post['post_usuario'] = $_SESSION['userlogin']['user_id'];
        $post['post_data'] = date('Y/m/d');
        unset($post['SendPostForm']);

        require('_models/AdminPosts.class.php');
        $cadastra = new AdminPosts;
        $cadastra->ExeCreate($post);

        if ($cadastra->getResult()):
            echo "<script>
                    alert('O canhoto foi inserido com sucesso!');
                    window.location.replace(\"painel.php\");
                    </script>";
        else:
            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
        endif;
    endif;
    ?>

    <div class="formularios">
        <div class="cadastro-form"> 
            <form class="cadastro-form-box" name="PostForm" action="" method="post" enctype="multipart/form-data">

                <label class="label">
                    <span class="field">Número da NF-e:</span>
                    <input class="form-espac form-control" type="tel" name="post_titulo" value="<?php if (isset($post['post_titulo'])) echo $post['post_titulo']; ?>" />
                </label>

                <label class="label">
                    <span class="field">Série:</span>
                    <input class="form-espac form-control" type="tel" name="post_tipo" value="<?php if (isset($post['post_tipo'])) echo $post['post_tipo']; ?>" />
                </label>

                <label class="label">
                    <span class="field">Número CT-e:</span>
                    <input class="form-espac form-control" type="tel" name="post_cte" value="<?php if (isset($post['post_cte'])) echo $post['post_cte']; ?>" />
                </label>


                <label class="label">
                    <span class="field">Chave do CT-e</span>
                    <input class="form-espac form-control" type="tel" name="post_chave" value="<?php if (isset($post['post_chave'])) echo $post['post_chave']; ?>" />
                </label>

                <label class="label">
                    <span class="field">Cliente:</span>
                    <select class="form-control" name="post_user">
                        <option value=""> Selecione um cliente: </option>                        
                        <?php
                        $readAut = new Read;
                        $readAut->ExeRead("users", "WHERE user_level = 2 ORDER BY user_name ASC");

                        if ($readAut->getRowCount() >= 1):
                            foreach ($readAut->getResult() as $aut):
                                echo "<option ";

                                if ($post['post_user'] == $aut['user_id']):
                                    echo "selected=\"selected\" ";
                                endif;

                                echo "value=\"{$aut['user_id']}\"> {$aut['user_name']} {$aut['user_lastname']} </option>";
                            endforeach;
                        endif;
                        ?>
                    </select>
                </label>

                <label class="label">
                    <span class="field">Imagem do Canhoto</span>
                    <input class="form form-control" type="file" name="post_img" />
                </label>



            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
                <input type="submit" class="btn" value="Publicar" name="SendPostForm" />

            </form>
        </div>
    </div>
</div> <!-- content home -->