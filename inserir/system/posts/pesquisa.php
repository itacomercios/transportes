<?php
$search = filter_input(INPUT_GET, 'filtro', FILTER_DEFAULT);
$inicio = filter_input(INPUT_GET, 'Inicio', FILTER_DEFAULT);
$fim = filter_input(INPUT_GET, 'Fim', FILTER_DEFAULT);

//$mpdf = new mPDF();
//$mpdf->SetDisplayMode('fullpage');

if (isset($search)):
    $linkto = explode('/', $search);
else:
    $linkto = array();
endif;


if (isset($inicio)):
    $datainicio = explode('/', $inicio);
else:
    $datainicio = array();
endif;

if (isset($fim)):
    $datafim = explode('/', $fim);
else:
    $datafim = array();
endif;

if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;

if ($search):
    ?>
    <section class="conteudo_lista">
        <h1>Pesquisa por <b><?= $search ?></b></h1>


        <div class="conteudo-tabela">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>NF-e</th>
                            <th>CT-e</th>
                            <th>Chave</th>
                            <th>Data</th>
                            <th>Views</th>
                            <th>Ação</th>
                        </tr>
                    </thead>

                    <?php
                    $readPesquisa = new Read;
                    $readPesquisa->ExeRead('posts', "WHERE post_status = 1 AND post_titulo LIKE '%' :link '%' ", "link={$search}");
                    if ($readPesquisa->getResult()):
                        foreach ($readPesquisa->getResult() as $post):
                            extract($post);
                            $status = (!$post_status ? 'style="background: #fffed8"' : '');
                            ?>
                            <tbody>
                                <tr>
                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td><?= $post_downloads ?></td>
                                    <td>
                                        <ul class='acao'>
                                            <li><a href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar" class="action user_edit">Editar</a></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
    </section>
    <?php
endif;

if ($inicio && $fim):
    ?>
    <section class="conteudo_lista">


        <div>
            <h3>Relatório por período: de <b><?= $datainicio[0] ?></b> até <b><?= $datafim[0] ?></b></h3>
            <form name="imprimir" action="" method="post">
                <input class = "btn blue" type = "submit" name = "imprimir" value = "Imprimir" />
            </form>

        </div>



        <div class="conteudo-tabela">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>NF-e</th>
                            <th>CT-e</th>
                            <th>Chave</th>
                            <th>Data</th>
                            <th>Views</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <?php
                    $readPesquisa = new Read;
                    $readPesquisa->ExeRead('posts', "WHERE post_data BETWEEN :inicio AND :fim ORDER BY post_user ASC, post_nome desc", "inicio={$datainicio[0]}&fim={$datafim[0]}");
                    if ($readPesquisa->getResult()):
                        foreach ($readPesquisa->getResult() as $post):
                            extract($post);
                            $status = (!$post_status ? 'style="background: #fffed8"' : '');
                            ?>
                            <tbody>
                                <tr>
                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td><?= $post_downloads ?></td>
                                    <td>
                                        <ul class='acao'>
                                            <li><a href="painel.php?exe=posts/update&postid=<?= $post_id; ?>" title="Editar" class="action user_edit">Editar</a></li>
            <!--                                        <li><a href="painel.php?exe=posts/index&post=<?= $post_id; ?>" title="Deletar" class="action user_dele">Deletar</a></li>-->
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </table>
            </div>
        </div>
    </div>
    </section>
    <?php
endif;
?>
<?php
$imprimir = filter_input(INPUT_POST, 'imprimir', FILTER_DEFAULT);
if (isset($imprimir)):
    header('Location: painel.php?exe=posts/gerarpdf&Inicio=' . $datainicio[0] . '&Fim=' . $datafim[0]);
endif;