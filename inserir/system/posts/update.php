<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="content form_create">
    <header>
        <h1>Atualizar Canhoto:</h1>
    </header>

    <?php
    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    $postid = filter_input(INPUT_GET, 'postid', FILTER_VALIDATE_INT);

    if (isset($post) && $post['SendPostForm']):
        $post['post_status'] = '1';
        $post['post_img'] = ( $_FILES['post_img']['tmp_name'] ? $_FILES['post_img'] : 'null' );
        unset($post['SendPostForm']);

        require('_models/AdminPosts.class.php');
        $cadastra = new AdminPosts;
        $cadastra->ExeUpdate($postid, $post);

        header('Location: painel.php?exe=posts/index');
        WSErro($cadastra->getError()[0], $cadastra->getError()[1]);


    else:
        $read = new Read;
        $read->ExeRead("posts", "WHERE post_id = :id", "id={$postid}");
        if (!$read->getResult()):
            header('Location: painel.php?exe=posts/index&empty=true');
        else:
            $post = $read->getResult()[0];
            $post['post_data'] = date('d/m/Y H:i:s', strtotime($post['post_data']));
        endif;
    endif;

    if (!empty($_SESSION['errCapa'])):
        WSErro($_SESSION['errCapa'], E_USER_WARNING);
        unset($_SESSION['errCapa']);
    endif;

    $checkCreate = filter_input(INPUT_GET, 'create', FILTER_VALIDATE_BOOLEAN);
    if ($checkCreate && empty($cadastra)):
        WSErro("O Documento referente a NF-e de nº <b>{$post['post_titulo']}</b> foi atualizado com sucesso no sistema!", WS_ACCEPT);
    endif;
    ?>


    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <label class="label">
            <span class="field">Imagem do Canhoto</span>
            <input type="file" name="post_img" />
        </label>

        <div class="label_line">


            <label class="label_small">
                <span class="field">Número da NF-e:</span>
                <input type="text" name="post_titulo" value="<?php if (isset($post['post_titulo'])) echo $post['post_titulo']; ?>" />
            </label>

            <label class="label_small">
                <span class="field">Série:</span>
                <input type="text" name="post_tipo" value="<?php if (isset($post['post_tipo'])) echo $post['post_tipo']; ?>" />
            </label>
            <label class="label_small">
                <span class="field">Número CT-e:</span>
                <input type="text" name="post_cte" value="<?php if (isset($post['post_cte'])) echo $post['post_cte']; ?>" />
            </label>
        </div>

        <label class="label">
            <span class="field">Chave do CT-e</span>
            <input type="text" name="post_chave" value="<?php if (isset($post['post_chave'])) echo $post['post_chave']; ?>" />
        </label>
        <div class="label_line">

            <label class="label_small">
                <span class="field">Data da Criação:</span>
                <input type="datetime" class="formDate center" name="post_data" value="<?php
                if (isset($post['post_data'])): echo $post['post_data'];
                else: echo date('d/m/Y');
                endif;
                ?>" />
            </label>

            <label class="label_small">
                <span class="field">Categoria:</span>
                <select name="post_categoria">
                    <!--                    <option value=""> Selecione a categoria: </option>                        -->
                    <?php
                    $readSes = new Read;
                    $readSes->ExeRead("categorias", "WHERE categoria_parent IS NULL AND categoria_nome = :doc ORDER BY categoria_titulo ASC", "doc=documentos");
                    if ($readSes->getRowCount() >= 1):
                        foreach ($readSes->getResult() as $ses):
//                            echo "<option disabled=\"disabled\" value=\"\"> {$ses['categoria_titulo']} </option>";
                            $readCat = new Read;
                            $readCat->ExeRead("categorias", "WHERE categoria_parent = :parent ORDER BY categoria_titulo ASC", "parent={$ses['categoria_id']}");

                            if ($readCat->getRowCount() >= 1):
                                foreach ($readCat->getResult() as $cat):
                                    echo "<option ";

                                    if ($post['post_categoria'] == $cat['categoria_id']):
                                        echo "selected=\"selected\" ";
                                    endif;

                                    echo "value=\"{$cat['categoria_id']}\"> {$cat['categoria_titulo']} </option>";
                                endforeach;
                            endif;

                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

            <label class="label_small">
                <span class="field">Selecione o Usuário:</span>
                <select name="post_user">
                    <option value=""> Selecione o usuário: </option>                        
                    <!--<option value="<?= $_SESSION['userlogin']['user_id']; ?>"> <?= "{$_SESSION['userlogin']['user_name']} {$_SESSION['userlogin']['user_lastname']}"; ?> </option>-->
                    <?php
                    $readAut = new Read;
                    $readAut->ExeRead("users", "WHERE user_level = 2 ORDER BY user_name ASC");

                    if ($readAut->getRowCount() >= 1):
                        foreach ($readAut->getResult() as $aut):
                            echo "<option ";

                            if ($post['post_user'] == $aut['user_id']):
                                echo "selected=\"selected\" ";
                            endif;

                            echo "value=\"{$aut['user_id']}\"> {$aut['user_name']} {$aut['user_lastname']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

        </div><!--/line-->

        <div class="label_line">

            <!--<input type="submit" class="btn blue" value="Cadastrar" name="SendPostForm" />-->
            <input type="submit" class="btn green" value="Atualizar" name="SendPostForm" />
        </div>
    </form>
</div> <!-- content home -->