<?php
if (empty($login)) :
    header('Location: ../../painel.php');
    die;
endif;
?>
<div class="home">
    <h1 class="boxtitle">Canhotos:</h1>
    <a class="btn" href="painel.php?exe=posts/create">Postar Canhoto</a>
    <section>
        <h1 class="boxsubtitle">Canhotos adicionados Recentemente:</h1>
        <?php
        $read = new Read;
        $read->ExeRead("posts", " ORDER BY post_data DESC LIMIT 10");
        if ($read->getResult()):
            ?>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NF-e</th>
                        <th>CT-e</th>
                        <th>Indústria</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <?php
                foreach ($read->getResult() as $re):
                    extract($re);
                    ?>
                    <tbody>
                        <tr class="lista-tabela">

                            <td><?= $post_titulo ?></td>
                            <td><?= $post_cte ?></td>
                            <td><?php
                                $industria = new Read;
                                $industria->ExeRead("users", "WHERE user_id = :id", "id={$post_user}");

                                echo $industria->getResult()[0]['user_name'];
                                ?>
                            </td>
                            <td><?= date("d M", strtotime($post_data)) ?></td>
                        </tr>
                    </tbody>

                    <?php
                endforeach;
                ?>
            </table>
            <?php
        endif;
        ?>
    </section>          
</div>
