<?php
$Email= filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (isset($Email) && isset($Email['Cadastrar'])):
    unset($Email["Cadastrar"]);

    require '_app/Models/CadastrarEmail.class.php';
    $cadastra = new CadastraEmail;
    $cadastra->ExeCreate($Email);

    if ($cadastra->getResult()):
        ?>
        <script>
            alert("Email Cadastrado com sucesso. Obrigado por assinar nossa NewsLetter!");
        </script>
        <?php
        
    endif;
else:
    

endif;
?>
<h3>Receba nossas notícias!</h3>
<form method="post" action="" id="newsletter">
    <input type="text" name="email_nome" placeholder="Como gostaria de ser Chamado?" id="nome">
    <input type="email" name="email_email" placeholder="Qual o seu melhor e-mail?" id="email">
    <input type="submit" value="Cadastrar" name="Cadastrar" class="btn btn-azul">
</form>
