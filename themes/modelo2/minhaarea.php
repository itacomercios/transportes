<?php
if ($Link->getData()):

    extract($Link->getData());

else:
    header("Location: " . HOME . DIRECTORY_SEPARATOR . '404');
endif;

$login = new Login(1);
$logoff = filter_input(INPUT_GET, 'logoff', FILTER_VALIDATE_BOOLEAN);


if (!$login->CheckLogin()):
    unset($_SESSION['userlogin']);
    header("Location: \caturama/index.php");
else:
    $userlogin = $_SESSION['userlogin'];
endif;

if ($logoff):
    unset($_SESSION['userlogin']);
    header("Location: HOME/index.php");
endif;

//Validação do Formulário para Atualizar Dados

$Dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (isset($Dados) && isset($Dados['FormAtualiza'])):
    unset($Dados['FormAtualiza'], $Dados['user_password']);
    $cadastra = new CadastrarUser;
    $cadastra->ExeUpdateCliente($userlogin['user_id'], $Dados);

    WSErro($cadastra->getError()[0], $cadastra->getError()[1]);

endif;

$visualizar = filter_input(INPUT_POST, FILTER_DEFAULT);
if (isset($visualizar) && isset($visualizar['visualizar'])):
//    header("Location:" . HOME . "/uploads/" . $post_img."?nome=".$post_nome);
    header("Location:" . HOME . "&" . $post_nome);


endif;


$nfe = filter_input(INPUT_POST, 'nfe', FILTER_DEFAULT);
$cte = filter_input(INPUT_POST, 'cte', FILTER_DEFAULT);
$chave = filter_input(INPUT_POST, 'chave', FILTER_DEFAULT);
?>
<section class="container-box shadow minhaarea">
    <h2>Olá <b><?= $userlogin['user_name'] . ' ' . $userlogin['user_lastname'] ?></b>, Seja bem vindo(a)!  -  <a class="btn" href="<?= HOME ?>/logoff">Sair</a></h2>

    <div class="procurar container">
        <div class="procurar-box ">
            <div class="procurar_nome">
                <label>Procure por NF-e!</label>
                <form method="post" name="procurar_nfe" action="">
                    <input type="text" placeholder="Pesquise por NF-e" name='nfe'/>
                    <input type="submit" value="Pesquisar" name="Pesquisar" class="search">
                </form>
            </div>

            <div class="procurar_bairro">
                <label>Procure por CT-e!</label>
                <form method="post" name="procurar_cte" action="">
                    <input type="text" placeholder="Pesquise por CT-e" name='cte'/>
                    <input type="submit" value="Pesquisar" name="Pesquisar" class="search">
                </form>
            </div>

            <div class="procurar_cidade">
                <label>Procure por Chave!</label>
                <form method="post" name="procurar_chave" action="">
                    <input type="text" placeholder="Pesquise por Chave" name='chave'/>
                    <input type="submit" value="Pesquisar" name="Pesquisar" class="search">
                </form>
            </div>
            <div>
                <a class="btn" href="<?= HOME . '/login/login' ?>">Tela Principal</a>
            </div>
        </div>
    </div>
    <?php
    if (isset($nfe)):
        ?>
        <div class="minhaarea-box">
            <div class="minhaarea-info">
                <?php
                $readPosts = new Read;
                $readPosts->ExeRead("posts", "WHERE post_status = 1 AND post_user =:user AND post_titulo = :nfe ORDER BY post_status ASC, post_data DESC", "nfe={$nfe}&user={$userlogin['user_id']}");
                if ($readPosts->getResult()):
                    ?>
                    <h3>Canhotos</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>NF-e</th>
                                <th>CT-e</th>
                                <th>Chave</th>
                                <th>Data</th>
                                <th>Ação</th>
                            </tr>
                        </thead>

                        <?php
                        foreach ($readPosts->getResult() as $canhotos):
                            extract($canhotos);
                            ?>
                            <tbody>
                                <tr class="lista-tabela">

                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td>
                                        <a class="baixar" target="_blank" href="<?= HOME ?>/uploads/<?= $post_img ?>" title="Visualizar Canhoto">Visualizar Canhoto</a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                        ?>


                    </table>

                    <?php
                endif;
                ?>
            </div>
        </div>
        <?php
    elseif (isset($cte)):
        ?>
        <div class="minhaarea-box">
            <div class="minhaarea-info">
                <?php
                $readPosts = new Read;
                $readPosts->ExeRead("posts", "WHERE post_status = 1 AND post_user =:user AND post_cte = :cte ORDER BY post_status ASC, post_data DESC", "cte={$cte}&user={$userlogin['user_id']}");
                if ($readPosts->getResult()):
                    ?>
                    <h3>Canhotos</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>NF-e</th>
                                <th>CT-e</th>
                                <th>Chave</th>
                                <th>Data</th>
                                <th>Ação</th>
                            </tr>
                        </thead>

                        <?php
                        foreach ($readPosts->getResult() as $canhotos):
                            extract($canhotos);
                            ?>
                            <tbody>
                                <tr class="lista-tabela">

                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td>
                                        <a class="baixar" target="_blank" href="<?= HOME ?>/uploads/<?= $post_img; ?>" title="Visualizar Canhoto">Visualizar Canhoto</a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                        ?>


                    </table>

                    <?php
                endif;
                ?>
            </div>
        </div>
        <?php
    elseif (isset($chave)):
        ?>
        <div class="minhaarea-box">
            <div class="minhaarea-info">
                <?php
                $readPosts = new Read;
                $readPosts->ExeRead("posts", "WHERE post_status = 1 AND post_user =:user AND post_chave LIKE '%' :chave '%' ORDER BY post_status ASC, post_data DESC", "chave={$chave}&user={$userlogin['user_id']}");
                if ($readPosts->getResult()):
                    ?>
                    <h3>Canhotos</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>NF-e</th>
                                <th>CT-e</th>
                                <th>Chave</th>
                                <th>Data</th>
                                <th>Ação</th>
                            </tr>
                        </thead>

                        <?php
                        foreach ($readPosts->getResult() as $canhotos):
                            extract($canhotos);
                            ?>
                            <tbody>
                                <tr class="lista-tabela">

                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td>
                                        <a class="baixar" target="_blank" href="<?= HOME ?>/uploads/<?= $post_img; ?>" title="Visualizar Canhoto">Visualizar Canhoto</a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                        ?>


                    </table>

                    <?php
                endif;
                ?>
            </div>
        </div>
        <?php
    else:
        ?>


        <div class="minhaarea-box">
            <div class="minhaarea-info">
                <?php
                $readPosts = new Read;
                $readPosts->ExeRead("posts", "WHERE post_status = 1 AND post_user =:user ORDER BY post_status ASC, post_data DESC", "user={$userlogin['user_id']}");
                if ($readPosts->getResult()):
                    ?>
                    <h3>Canhotos</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>NF-e</th>
                                <th>CT-e</th>
                                <th>Chave</th>
                                <th>Data</th>
                                <th>Ação</th>
                            </tr>
                        </thead>

                        <?php
                        foreach ($readPosts->getResult() as $canhotos):
                            extract($canhotos);
                            ?>
                            <tbody>
                                <tr class="lista-tabela">

                                    <td><?= $post_titulo ?></td>
                                    <td><?= $post_cte ?></td>
                                    <td><?= $post_chave ?></td>
                                    <td><?= date('d/m/Y', strtotime($post_data)); ?></td>
                                    <td>
                                        <a class="baixar" target="_blank" href="<?= HOME ?>/uploads/<?= $post_img ?>?nome=<?= $post_nome ?>" title="Visualizar Canhoto">Visualizar Canhoto</a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php
                        endforeach;
                        ?>


                    </table>

                    <?php
                endif;
                ?>
            </div>
        </div>

    <?php
    endif;
    ?>
</section>