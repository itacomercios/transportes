<?php
if ($Link->getData()):

    extract($Link->getData());

else:
    header("Location: " . HOME . DIRECTORY_SEPARATOR . '404');
endif;
?>
<section class="container-box contato">

    <h2 class="titulo">Entre em Contato Conosco!</h2>

    <div class="contato-container">
        <div class="contato-form"> 
            <?php
            $readContato = new Read;
            $readContato->ExeRead('contato');
            if ($readContato->getResult()):

                $email = $readContato->getResult()[0]['contato_email'];
            endif;

            $Contato = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if ($Contato && $Contato['SendFormContato']):
                unset($Contato['SendFormContato']);
                $Contato['Assunto'] = $Contato['RemetenteAssunto'];
                $Contato['DestinoNome'] = SITENAME;
                $Contato['DestinoEmail'] = $email;

                $SendMail = new Email;
                $SendMail->Enviar($Contato);

                if ($SendMail->getError()):
                    WSErro($SendMail->getError()[0], $SendMail->getError()[1]);
                endif;
            endif;
            ?>

            <form name="FormContato" method="post">
                <div class="contato-geral">
                    <div class="contato-input">
                        <input type="text" class="form-control" id="nome" name="RemetenteNome" placeholder="Digite seu Nome">
                        <input type="email" class="form-control" id="email" name="RemetenteEmail" placeholder="Digite seu e-mail!">
                        <input type="text" class="form-control" id="assunto" name="RemetenteAssunto" placeholder="Assunto">
                    </div>
                    <textarea class="form-control" id="mensagem" name="Mensagem" rows="5" placeholder="Mensagem"></textarea>
                </div>
                <div class="contato-botao">
                    <input type="submit" value="Enviar Email" name="SendFormContato" class="btn btn-azul"> 
                </div>
            </form>
        </div>

        <div class="contato-detalhes">
            <?php
            $readContato = new Read;
            $readContato->ExeRead('contato');
            if ($readContato->getResult()):
                extract($readContato->getResult()[0]);
                ?>
                <div class="contato-info">
                    <!--<span class="glyphicon glyphicon-map-marker"></span>-->
                    <span><img src="<?= INCLUDE_PATH ?>/img/icones/localizacao.png"></span>
                    <p><?= $contato_endereco . "<br>" . $contato_bairro . "<br>" . $contato_cidade . " - " . $contato_estado ?></p>
                </div>

                <div class="contato-info">
                    <!--<span class="glyphicon glyphicon-earphone"></span>-->	
                    <span><img src='<?= INCLUDE_PATH ?>/img/icones/telefone.png'></span>
                    <p><?= $contato_telefone ?></p>
                </div>

                <a href="https://api.whatsapp.com/send?phone=<?= $contato_celular ?>" target="_blank" class="contato-info">
                            <!--<span class="glyphicon glyphicon-earphone"></span>-->	
                    <span><img src='<?= INCLUDE_PATH ?>/img/icones/whatsapp.png'></span>
                    <p><?= $contato_celular ?></p>
                </a>

                <div class="contato-info">
                        <!--<span class="glyphicon glyphicon-earphone"></span>-->	
                    <span><img src='<?= INCLUDE_PATH ?>/img/icones/email.png'></span>
                    <p><?= $contato_email ?></p>
                </div>

                <?php
            endif;
            ?>

        </div>
    </div>
</section>