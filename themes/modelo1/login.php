<?php
if ($Link->getData()):

    extract($Link->getData());

else:
    header("Location: " . HOME . DIRECTORY_SEPARATOR . '404');
endif;
?>
<section class="cadastrar container-box">
    <?php
    $id = 3;
    $readMsg = new Read;
    $readMsg->ExeRead('informacoes', "WHERE info_status = 1 AND info_id = :id", "id={$id}");
    if ($readMsg->getResult()):
        extract($readMsg->getResult()[0]);
        ?>
        <h1><?= $info_subtitulo ?></h1>

        <?php
    endif;
    ?>

    <?php
    $login = new Login(1);


// Se clicar na tela de login e estiver logado, executa este código
    if ($login->CheckLogin()):

        $meuId = $_SESSION['userlogin']['user_id'];
        header("Location: ../minhaarea/$meuId");
    endif;

    $dataLogin = filter_input_array(INPUT_POST, FILTER_DEFAULT);


// A Primeira vez que faz o login executa este código
    if (!empty($dataLogin['UserLogin'])):
        $login->ExeLogin($dataLogin);
        if (!$login->getResult()):
            WSErro($login->getError()[0], $login->getError()[1]);
        else:
            $meuId = $_SESSION['userlogin']['user_id'];
            header("Location: ../minhaarea/$meuId");
        endif;
    endif;

    $get = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);
    if (!empty($get)):
        if ($get == 'restrito'):
            WSErro('<b>Oppsss </b> Acesso Negado, favor efetuar login para ter acesso!', WS_ALERT);
        elseif ($get == 'logoff'):
            WSErro('<b>Sucesso ao deslogar </b> Sua sessão foi finalizada com sucesso!', WS_ACCEPT);
        endif;
    endif;
    ?>

    <div class="formularios">
        <div class="cadastro-form"> 
            <form class="cadastro-form-box" name="FormLogin" method="post">
                <h3>Faça seu Login</h3>
                <div class="form-linha">
                    <input class="um form-espac" type="email" class="form-control" id="user" name="user" placeholder="Digite seu Email">
                </div>

                <div class="form-linha">
                    <input class="tres form-espac" type="password" class="form-control" id="pass" name="pass" placeholder="Digite sua senha">
                </div>

                <div class="contato-botao">
                    <input type="submit" value="Entrar" name="UserLogin" class="btn btn-azul">
                </div>
            </form>
        </div>
    </div>
</section>