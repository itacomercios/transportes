<?php
$View = new View;

$banner_tpl = $View->Load('slide');
?>
<section class="container-slide">
    <div class="introducao slide container-box ">
        <?php
        $slide = new Read;
        $slide->ExeRead('banner', "WHERE banner_status = 1;");
        if ($slide->getResult()):
            foreach ($slide->getResult() as $banner):
                $View->Show($banner, $banner_tpl);
            endforeach;
        endif;
        ?>
    </div>
</section>


<section class="container-box quem-somos" id="quem-somos">
    <?php
    $readCat = new Read;
    $readCat->ExeRead('categorias', "WHERE categoria_nome = :cat", "cat=empresas");
    if ($readCat->getResult()):
        ?>
        <h1 class="titulo">Quem Somos</h1>
        <div><?= $readCat->getResult()[0]['categoria_conteudo'] ?></div>
        <?php
    endif;
    ?>
    <div class="item">
        <?php
        $readSobre = new Read;
        $readSobre->ExeRead('empresa', "WHERE emp_status = 1");
        if ($readSobre->getResult()):
            foreach ($readSobre->getResult() as $infos):
                ?>
                <div class="item-box">
                    <div class="item-img">
                        <img src="<?= HOME.'/uploads/'.$infos['emp_img']?>" alt="<?= $infos['emp_titulo']?>" title="<?= $infos['emp_titulo']?>">
                    </div>
                    <div class="item-info">
                        <h2><?= $infos['emp_titulo'] ?></h2>
                        <div><?= $infos['emp_conteudo'] ?></div>        
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </div>
    <?php
    ?>
</section>


<section class="container-box contato" data-group="contato" id="contato">

    <h2 class="titulo">Entre em Contato Conosco!</h2>

    <div class="contato-container">
        <div class="contato-form"> 
            <h3>Preencha o formulário</h3>	
            <?php
            $Contato = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if ($Contato && $Contato['SendFormContato']):
                unset($Contato['SendFormContato']);
                $Contato['Assunto'] = 'Mensagem via formulário do Site';
                $Contato['DestinoNome'] = 'Transportes XXXXXXXXX';
                $Contato['DestinoEmail'] = 'contato@fladermorais.com.br';

                $SendMail = new Email;
                $SendMail->Enviar($Contato);

                if ($SendMail->getError()):
                    WSErro($SendMail->getError()[0], $SendMail->getError()[1]);
                endif;
            endif;
            ?>

            <form name="FormContato" method="post">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="RemetenteNome" placeholder="Digite seu Nome">
                </div>

                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="RemetenteEmail" placeholder="Digite seu e-mail!">
                </div>

                <div class="form-group">
                    <label for="assunto">Assunto</label>
                    <input type="text" class="form-control" id="assunto" name="RemetenteAssunto" placeholder="Assunto">
                </div>

                <div class="form-group">
                    <label for="mensagem">Mensagem</label>
                    <textarea class="form-control" id="mensagem" name="Mensagem" rows="5"></textarea>
                </div>


                <input type="submit" value="Enviar Email" name="SendFormContato" class="btn btn_verde"> 
            </form>
        </div>

        <div class="contato-detalhes">
            <h3>Onde Estamos</h3>
            <?php
            $readContato = new Read;
            $readContato->ExeRead('contato');
            if ($readContato->getResult()):
                extract($readContato->getResult()[0]);
                ?>
                <img class="img-responsive img-rounded" src="<?= HOME ?>/uploads/<?= $contato_img ?>">

                <div class="contato-info">
                    <span class="glyphicon glyphicon-map-marker"></span>	
                    <p><?= $contato_endereco . "<br>" . $contato_bairro . "<br>" . $contato_cidade . " - " . $contato_estado ?></p>
                </div>

                <div class="contato-info">
                    <span class="glyphicon glyphicon-earphone"></span>	
                    <p><?= $contato_telefone ?><br><?= $contato_celular ?></p>
                </div>

                <?php
            endif;
            ?>

        </div>
    </div>
</section>