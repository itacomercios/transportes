<section>
    <div class="introducao slide">
        <a href="#">
            <img src="<?= INCLUDE_PATH ?>/img/banners/01.jpg" alt="imagem 01 do slide">
            <h2>Imagem 01</h2>
        </a> 

        <a href="#">
            <img src="<?= INCLUDE_PATH?>/img/banners/02.jpg" alt="imagem 02 do slide">
            <h2>Imagem 02</h2>
        </a>

        <a href="#">
            <img src="<?= INCLUDE_PATH ?>/img/banners/03.jpg" alt="imagem 03 do slide">
            <h2>Imagem 03</h2>
        </a>
        
        <a href="#">
            <img src="<?= INCLUDE_PATH ?>/img/banners/04.jpg" alt="imagem 04 do slide">
            <h2>Imagem 04</h2>
        </a>
    </div>
</section>