//  Incluir o Active no Menu
$('.icones a').each(function(){
	var height = $(this).height(),
		offsetTop = $(this).offset().top,
		menuHeight = $('.menu').innerHeight(),
		id = $(this).attr('id'),
		$itemMenu = $('a[href="#' + id + '"]');
                

	$(window).scroll(function(){
		var scrollTop = $(window).scrollTop(),
			distanciaTopo = offsetTop - menuHeight,
			distanciaBotton = offsetTop + height - menuHeight;

		if (distanciaTopo < scrollTop && distanciaBotton > scrollTop){
			$itemMenu.addClass('active');
		} else {
			$itemMenu.removeClass('active');
		}
	});
});

$('.voltar_topo a').click(function(e){
	e.preventDefault();
	$('html, body').animate({
		scrollTop: 0
	}, 500);
});

$('.icones a').click(function(){
   $(this).toggleClass('active');
});


$('.mobile-btn').click(function(){
   $(this).toggleClass('active');
    $('.mobile-menu').toggleClass('active');
});


$('[data-group]').each(function(){
	var $allTarget = $(this).find('[data-target]'),
			$allClick = $(this).find('[data-click]'),
			activeClass = 'active';
	
	$allTarget.first().addClass(activeClass);
	$allClick.first().addClass(activeClass);
	
	$allClick.click(function(e){
		e.preventDefault();
		
		var id = $(this).data('click'),
				$target = $('[data-target="' + id + '"]');
		
		$allClick.removeClass(activeClass);
		$allTarget.removeClass(activeClass);
		
		$target.addClass(activeClass);
		$(this).addClass(activeClass);
	});
});

// ******************* //
//       SLIDE         //
// ******************* //

//Ativa e mostra o primeiro slide
$(".slide > :first").addClass("active");

// Função para o slide rodar
function rotateSlide() {
    var activeSlide = $('.slide > .active'),
        nextSlide   = activeSlide.next();
  
    if (nextSlide.length == 0) {
        nextSlide = $('.slide > :first');
    }
    
    activeSlide.removeClass('active');
    nextSlide.addClass('active');
}
// Chama a função slide
setInterval(rotateSlide, 4000);


// ******************* //
//       SLIDE         //
// ******************* //
$('.menu-nav a[href^="#"]').click(function(e){
	e.preventDefault();
	var id = $(this).attr('href'),
			menuHeight = $('.menu').innerHeight(),
			targetOffset = $(id).offset().top;
	
	$('html, body').animate({
		scrollTop: targetOffset - menuHeight
	}, 1000);
});