<?php

$modelo = filter_input(INPUT_GET, 'modelo', FILTER_DEFAULT);
if (isset($modelo)):
    $modelo = $modelo;
else:
    $modelo ='modelo1';
endif;
// CONFIGRAÇÕES DO BANCO ####################
define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DBSA', 'transportes');

// DEFINE SERVIDOR DE E-MAIL ####################
define('MAILUSER','contato@fladermorais.com.br');
define('MAILPASS','fladerf1a2m3');
define('MAILPORT','587');
define('MAILHOST','mail.fladermorais.com.br');

// DEFINE IDENTIDADE DO SITE ####################
define('SITENAME',' Transportadora X ');
define('SITEDESC','Sistema de Canhotos para Transportadora');

// DEFINE A BASE DO SITE ####################
define('HOME','http://localhost/transportes');
define('BASE','http://localhost/transportes');
define ('THEME',$modelo);

define('INCLUDE_PATH', BASE . DIRECTORY_SEPARATOR. 'themes' . DIRECTORY_SEPARATOR . THEME);
define('REQUIRE_PATH','themes'. DIRECTORY_SEPARATOR . THEME);

date_default_timezone_set('America/Sao_Paulo');

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {
    $cDir = ['Conn', 'Helpers', 'Models', 'Pdf'];
    $iDir = null;
    foreach ($cDir as $dirName):
        if (!$iDir && file_exists(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php') && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php')):
            include_once (__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php');
            $iDir = true;
        endif;
    endforeach;

    if (!$iDir):
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    endif;
}
// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');

//WSErro :: Exibe erros lançados :: Front
function WSErro($ErrMsg, $ErrNo, $ErrDie = null) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

    if ($ErrDie):
        die;
    endif;
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">";
    echo "<b>Erro na Linha: #{$ErrLine} ::</b> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}
set_error_handler('PHPErro');